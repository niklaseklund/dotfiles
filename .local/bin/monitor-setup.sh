#!/usr/bin/env bash

if [ "$(xrandr -q | grep " connected" | wc -l)" -eq "1" ]; then
    # no external monitor connected, adjust to hidpi settings
    
    # hidpi settings
    export GDK_SCALE=1.5
    export GDK_DPI_SCALE=0.5
    export QT_SCREEN_SCALE_FACTORS=0.9
    export QT_AUTO_SCREEN_SCALE_FACTOR=2
    export QT_SCALE_FACTOR=2

    xrandr --output eDP-1 --dpi 227
else
    xrandr --output eDP-1 --dpi 227 --scale 0.75x0.75
    xrandr --output DP-2 --right-of eDP-1 --auto
fi
