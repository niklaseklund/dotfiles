# Required to refresh the prompt after fzy
bind -m emacs-standard '"\er": redraw-current-line'

# History
__fzy_history__() {
  local output
  output=$(cat ~/.bash_history | fzy)
  READLINE_LINE=${output#*$'\t'}
  if [ -z "$READLINE_POINT" ]; then
    echo "$READLINE_LINE"
  else
    READLINE_POINT=0x7fffffff
  fi
}

# Ctrl-r
bind -m emacs-standard '"\C-r": "\C-e \C-u\C-y\ey\C-u"$(__fzy_history__)"\e\C-e\er"'

# Open files
__fzy_recursive_open__() {
   local file 
   file=$(fd | fzy) && nvim "$file"
}

__fzy_open__() {
   local file 
   file=$(ls -a | fzy) && nvim "$file"
}

# Open files
alias ff=__fzy_recursive_open__
alias f=__fzy_open__
