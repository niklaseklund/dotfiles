#!/usr/bin/env bash

# Keyboardio device id
keyboard_device="04d9:0167"
lsusb_output="$(lsusb -d $keyboard_device)"
setxkbmap -layout us,se -option grp:shifts_toggle,ctrl:nocaps
# setxkbmap -layout us,se -option grp:shifts_toggle,ctrl:nocaps,altwin:swap_alt_win
if [ -z "$lsusb_output" ]; then
    # Keyboardio not connected
    xcape &
fi
