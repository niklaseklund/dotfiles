;; Nyxt configuration for version 2.0


;;
;; Keybindings
(define-configuration (buffer web-buffer)
  ((default-modes (append '(vi-normal-mode) %slot-default%))))

;;
;; Git
;; (setf nyxt/vcs:*vcs-projects-roots* '("~/opensource" "~/src"))
;; (setf nyxt/vcs:*vcs-usernames-alist* '(("github.com" . "niklascarlsson")
;;                                        ("gitlab.com" . "niklaseklund")))

;;
;; Browse
;; (define-configuration browser
;;   ;; restore session
;;   ((session-restore-prompt :always-restore)
;;    ;; search engines
;;    (search-engines
;;     (append
;;      (list
;;       (make-search-engine "quickdocs" "http://quickdocs.org/search?q=~a" "http://quickdocs.org/")
;;       (make-search-engine "wiki" "https://en.wikipedia.org/w/index.php?search=~a" "https://en.wikipedia.org/")
;;       (make-search-engine "yt" "https://www.youtube.com/results?search_query=~a" "https://www.youtube.com/")
;;       (make-search-engine "aw"  "https://wiki.archlinux.org/index.php?search=~a" "https://wiki.archlinux.org/"))
;;      %slot-default))))
