""
"" Vim core settings
syntax enable " Enable syntax highlighting
set number " Show line number
set noswapfile " Disables swapfile
set mouse=a "Enable mouse interaction
set clipboard+=unnamedplus " Enable clipboard paste from other sources
set tabstop=4 shiftwidth=4 expandtab
set ignorecase smartcase " smart case enabled

""
"" Colors
set noshowmode
if $THEME == 'light'
    set background=light
endif
if $THEME == 'dark'
    set background=dark
endif
set termguicolors

""
"" Keybindings
" Harmonize Y behavior with (C, D)
map Y y$
