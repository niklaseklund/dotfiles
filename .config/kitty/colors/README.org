* How to change theme

#+begin_src sh
  kitty @ set-colors --all --configured ~/.config/kitty/colors/operandi.conf
#+end_src
