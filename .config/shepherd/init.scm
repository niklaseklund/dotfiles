(use-modules (shepherd service)
             ((ice-9 ftw) #:select (scandir)))

;; Load all the files in the directory 'init.d' with a suffix '.scm'.
(for-each
  (lambda (file)
    (load (string-append "init.d/" file)))
  (scandir (string-append (dirname (current-filename)) "/init.d")
           (lambda (file)
             (string-suffix? ".scm" file))))

;; Load host config file from directory 'host'
(let* ((dir (dirname (current-filename)))
       (host-file (string-append dir "/host/" (gethostname) ".scm")))
  (if (file-exists? host-file)
      (load host-file)))

;; Send shepherd into the background
(action 'shepherd 'daemonize)
