;; Auto-mount
(start auto-mount)

;; PulseAudio
(start pulseaudio)

;; Cron
(start cron)

;; Trezor
(start trezor)

;; VPN
(start vpn)
