(define pulseaudio
  (make <service>
    #:provides '(pulseaudio)
    #:start (make-system-constructor "pulseaudio --start")
    #:stop (make-system-destructor "pulseaudio --kill")
    #:respawn? #t))
(register-services pulseaudio)
