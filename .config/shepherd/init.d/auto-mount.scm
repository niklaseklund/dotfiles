(define auto-mount
  (make <service>
    #:provides '(auto-mount)
    #:start (make-system-constructor "udiskie &")
    #:stop (make-system-destructor "pkill udiskie")
    #:respawn? #t))
(register-services auto-mount)
