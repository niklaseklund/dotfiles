(define dunst
  (make <service>
    #:docstring '("Dunst daemon")
    #:provides '(dunst)
    #:start (make-forkexec-constructor
             (list (string-join (list "dunst"))
                   "-print")
             #:log-file (string-append (getenv "HOME") "/.local/log/dunst.log"))
    #:stop
    (make-kill-destructor)
    #:respawn? #f))
(register-services dunst)

(start dunst)
