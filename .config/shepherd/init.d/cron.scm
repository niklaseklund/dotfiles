(define cron
  (make <service>
    #:provides '(cron)
    #:docstring "Crontab manager.
Start after PATH is fully set or else local programs could
be missing."
    #:start (make-system-constructor "mcron &")
    #:stop (make-system-destructor "pkill mcron")
    #:respawn? #t))
(register-services cron)
