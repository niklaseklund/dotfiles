(define auto-lock
  (make <service>
    #:provides '(auto-lock)
    #:start (make-forkexec-constructor
             '("xautolock" "-time" "15" "-locker" "slock"))
    #:stop (make-kill-destructor)
    #:respawn? #t))
(register-services auto-lock)
