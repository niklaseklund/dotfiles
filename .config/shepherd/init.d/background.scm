(define background
  (make <service>
    #:docstring "Set the desktop background."
    #:provides '(background)
    #:start (make-system-constructor
             (string-join `("feh"
                            "--no-fehbg"
                            "--bg-fill"
                            ,(string-join `(,(getenv "HOME") "/wallpaper_" ,(getenv "THEME")) ""))))
    #:one-shot? #t))

(register-services background)

(start background)
