(define redshift
  (make <service>
    #:provides '(redshift)
    #:requires '()
    #:docstring "Run `redshift'"
    #:start (make-forkexec-constructor
             '("redshift" "-v")
             #:log-file (string-append (getenv "HOME") "/.local/log/redshift.log"))
    #:stop (make-kill-destructor)))
(register-services redshift)

(start redshift)
