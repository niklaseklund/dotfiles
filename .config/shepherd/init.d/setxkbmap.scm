(define setxkbmap
  (make <service>
    #:docstring "Set keymap for X."
    #:provides '(setxkbmap)
    #:start (make-system-constructor
             (string-join '("setxkbmap"
                            "-layout" "us,se")))
    #:one-shot? #t))

(register-services setxkbmap)

(start setxkbmap)
