(define trezor
  (make <service>
    #:provides '(trezor)
    #:requires '()
    #:docstring "Run `trezord'"
    #:start (make-forkexec-constructor
             '("trezord-go")
             #:log-file (string-append (getenv "HOME") "/.local/log/trezord.log"))
    #:stop (make-kill-destructor)))
(register-services trezor)

(start trezor)
