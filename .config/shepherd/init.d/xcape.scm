(define xcape
  (make <service>
    #:requires '(setxkbmap)
    #:provides '(xcape)
    #:start (make-system-constructor "xcape -e 'Control_L=Escape'")
    #:stop (make-system-destructor "pkill xcape")
    #:respawn? #t))
(register-services xcape)
