(define emacs
  (make <service>
    #:provides '(emacs)
    #:requires '()
    #:docstring "Run `emacs' as a server"
    #:start (make-system-constructor "emacs --daemon")
    #:stop (make-system-destructor "emacsclient --eval \"(kill-emacs)\"")))
(register-services emacs)

(start emacs)
