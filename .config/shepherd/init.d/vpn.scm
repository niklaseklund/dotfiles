(define vpn
  (make <service>
    #:docstring "VPN Connection"
    #:provides '(vpn)
    #:start (make-system-constructor "nmcli connection up ${VPN_CONNECTION} passwd-file ${VPN_PASSWD_FILE} &")
    #:stop (make-system-destructor "nmcli connection down ${VPN_CONNECTION}")))

(register-services vpn)
