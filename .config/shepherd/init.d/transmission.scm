(define transmission
  (make <service>
    ;; Add require to vpn service
    #:provides '(transmission)
    #:start (make-forkexec-constructor
             `("transmission-daemon" "--foreground"
               "--logfile" ,(string-append (getenv "HOME") "/.local/log/transmission.log")))
    #:stop (make-kill-destructor)
    #:respawn? #t))
(register-services transmission)
