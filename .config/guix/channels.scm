(cons* (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
          "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
          (openpgp-fingerprint
           "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
       (channel
        (name 'neguix)
        (url "https://gitlab.com/niklaseklund/neguix")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
          "a4658f07d11e9e571bc70a5461fbfd2a2dac76c0"
          (openpgp-fingerprint
           "66E6 01AC 1756 020B 759B  E34B 7B65 F79C 3247 8510"))))
       (channel
        (name 'flat)
        (url "https://github.com/flatwhatson/guix-channel.git")
        (introduction
         (make-channel-introduction
          "33f86a4b48205c0dc19d7c036c85393f0766f806"
          (openpgp-fingerprint
           "736A C00E 1254 378B A982  7AF6 9DBE 8265 81B6 4490"))))
       %default-channels)
