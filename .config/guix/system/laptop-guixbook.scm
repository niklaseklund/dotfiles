;; This is an operating system configuration template
;; for a "desktop" setup with GNOME and Xfce where the
;; root partition is encrypted with LUKS.

(use-modules (gnu)
             (gnu system nss)
             (gnu services virtualization)
             (gnu services nix)         ;for nix package manager
             (gnu services sddm)        ;for login manager
             (gnu packages android)     ;for android-udev-rules
             (gnu packages finance)     ;for trezord-udev-rules
             (gnu system shadow)        ;for user-group
             (gnu packages linux)       ;for bluez
             (gnu packages package-management) ;for nix
             (nongnu packages linux)
             (nongnu system linux-initrd)
             (negnu services mbpfan)
             (ice-9 rdelim)
             (ice-9 format)
             (srfi srfi-1))
(use-service-modules desktop networking xorg base)
(use-package-modules certs gnome wm display-managers)

;; Allow members of the "video" group to change the screen brightness.
(define %backlight-udev-rule
  (udev-rule
   "90-backlight.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chgrp video /sys/class/backlight/%k/brightness\""
                  "\n"
                  "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chmod g+w /sys/class/backlight/%k/brightness\"")))

;; Use the "desktop" services
(define %my-services
  (cons*
   (modify-services
    %desktop-services
    (udev-service-type
     config =>
     (udev-configuration
      (inherit config)
      (rules (append (cons*
                      %backlight-udev-rule
                      android-udev-rules
                      trezord-udev-rules
                      (udev-configuration-rules config))))))
    (network-manager-service-type config =>
                                  (network-manager-configuration
                                   (inherit config)
                                   (vpn-plugins (list
                                                 network-manager-openvpn)))))))

(operating-system
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (cons broadcom-bt-firmware (list linux-firmware)))
  (host-name "guixbook")
  (timezone "Europe/Stockholm")
  (locale "en_US.utf8")

  ;; Choose US English keyboard layout.
  (keyboard-layout (keyboard-layout "us"))

  ;; Use the UEFI variant of GRUB with the EFI System
  ;; Partition mounted on /boot/efi.
  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets '("/boot/efi"))
               (keyboard-layout keyboard-layout)))

  ;; Kernel parameters
  (kernel-arguments '("rootflags=compress-force=zstd,subvol=@"
                      "modprobe.blacklist=usbmouse"))

  ;; Specify a mapped device for the encrypted root partition.
  ;; The UUID is that returned by 'cryptsetup luksUUID'.
  (mapped-devices
   (list (mapped-device
          (source (uuid "30015a10-9085-4f1c-9358-11905b562df5"))
          (target "root")
          (type luks-device-mapping))))

  (file-systems (append
                 (list
                  (file-system
                    (device (file-system-label "root"))
                    (mount-point "/")
                    (type "btrfs")
                    (options "subvol=@,compress-force=zstd")
                    (dependencies mapped-devices))
                  (file-system
                    (device (file-system-label "root"))
                    (mount-point "/home")
                    (type "btrfs")
                    (options "subvol=@home,compress-force=zstd")
                    (dependencies mapped-devices))
                  (file-system
                    (device (uuid "67E3-17ED" 'fat))
                    (mount-point "/boot/efi")
                    (type "vfat")))
                 %base-file-systems))

  (swap-devices '("/swapfile"))

  (users (cons (user-account
                (name "niklas")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"
                                        "lp" "adbusers"
                                        "dialout")))
               %base-user-accounts))

  (groups (cons (user-group (system? #t) (name "adbusers"))
                %base-groups))

  ;; This is where we specify system-wide packages.
  (packages (append (list
                     nss-certs                   ;for HTTPS access
                     gvfs                        ;for user mounts
                     guix-simplyblack-sddm-theme ;for sddm
                     stumpwm+slynk                     ;for window manager
                     bluez
                     nix
                     trezord)
                    %base-packages))

  ;; Add GNOME dekstop
  ;; Use the "desktop" services, which
  ;; include the X11 log-in service, networking with
  ;; NetworkManager, and more.
  (services (append (list (service gnome-desktop-service-type)
                          (service mbpfan-service-type
                                   (mbpfan-configuration
                                    (verbose? #t)))
                          (bluetooth-service)
                          (service nix-service-type)
                          ;; Add sddm login manager
                          (service sddm-service-type
                                   (sddm-configuration
                                    (theme "guix-simplyblack-sddm")))
                          ;; (set-xorg-configuration
                          ;;  (xorg-configuration
                          ;;   (keyboard-layout keyboard-layout)))
                          ;; Enable qemu emulation service
                          (service qemu-binfmt-service-type
                                   (qemu-binfmt-configuration
                                    (platforms (lookup-qemu-platforms "arm" "aarch64" "mips64el")))))
                    ;; %my-services
                    ;; Remove gdm login manager
                    (remove (lambda (service)
                              (member (service-kind service)
                                      (list
                                       gdm-service-type)))
                            %my-services)))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
