;;; ne-fd.el --- Fd -*- lexical-binding: t -*-

;;; Commentary:

;; This package provides commands using the tool fd.

;;; Code:

;;;; Requirements

(require 'project)

;;;; Variables

(defvar ne/fd-args '("--color=never" "--exclude" ".git*" "--hidden")
  "Arguments to pass to fd.")
(defvar ne/fd-program "fd"
  "Name of the executable.")
(defvar ne/fd-history nil
  "History for fd.")

;;;; Commands

;;;###autoload
(defun ne/fd-project ()
  "Run fd from project-root.

If not in a project run fd from `default-directory'."
  (interactive)
  (let* ((default-directory
           (ne/util-project-root))
         (file (ne/fd-file)))
    (find-file file)))

;;;###autoload
(defun ne/fd-other-project (project-root)
  "Run fd from another project-root."
  (interactive
   (list (completing-read
          "Select project: "
          (project-known-project-roots))))
  (let* ((default-directory project-root))
    (find-file
     (ne/fd-file))))

;;;###autoload
(defun ne/fd-directory ()
  "Run fd from directory."
  (interactive)
  (find-file
   (ne/fd-file)))

;;;###autoload
(defun ne/fd-other-directory (directory)
  "Run fd from other directory."
  (interactive "DSelect directory: ")
  (let* ((default-directory directory))
    (ne/fd-directory)))

;;;###autoload
(defun ne/fd-switch-to-project-directory (project)
  "Switch to directory in PROJECT."
  (interactive
   (list (ne/util-project-root)))
  (let* ((default-directory project)
         (args
          `(,@ne/fd-args "--type" "directory")))
    (find-file
     (ne/fd-file args))))

;;;; Functions

(defun ne/fd-file (&optional args)
  "Return files found with `fd' with FD-ARGS."
  (let ((args (if args args ne/fd-args)))
    (ne/fd-select-candidate
     (ne/fd--find args))))

(defun ne/fd-select-candidate (candidates)
  "Select a candidate from CANDIDATES."
  (completing-read "Select candidate: "
                   (lambda (str pred action)
                     (pcase action
                       ('metadata '(metadata (category . file)))
                       (`(boundaries . ,_) nil)
                       ('() (try-completion str candidates pred))
                       ('t (all-completions str candidates pred))
                       (_ (test-completion str candidates pred))))
                   nil t nil 'ne/fd-history))

;;;; Support functions

(defun ne/fd--find (command)
  "Execute COMMAND and return findings."
  (with-temp-buffer
    (with-connection-local-variables
     (apply #'process-file `(,ne/fd-program nil t nil ,@command))
     (split-string (buffer-string) "\n" t))))

(provide 'ne-fd)

;;; ne/fd.el ends here
