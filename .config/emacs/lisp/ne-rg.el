;;; ne-rg.el --- Ripgrep -*- lexical-binding: t -*-

;;; Commentary:

;; This package provides commands using the tool rg.

;;; Code:

;;;; Requirements

(require 'consult)
(require 'ne-project)

;;;; Variables

(defvar ne/rg-command "rg --null --line-buffered --color=never --max-columns=1000 --path-separator /\
   --smart-case --no-heading --line-number --hidden --glob=!.git/** ."
  "Arguments for rg")

;;;; Commands

;;;###autoload
(defun ne/rg-roam-directory ()
  "Search in `org-roam' notes."
  (interactive)
  (ne/rg-directory
   org-roam-directory))

;;;###autoload
(defun ne/rg-project (project)
  "Ripgrep in PROJECT."
  (interactive
   (list (ne/util-project-root)))
  (let ((default-directory project))
    (ne/rg--search)))

;;;###autoload
(defun ne/rg-directory (directory)
  "Ripgrep in DIRECTORY"
  (interactive
   (list default-directory))
  (ne/rg--search directory))

;;;###autoload
(defun ne/rg-other-directory (directory)
  "Ripgrep in other DIRECTORY."
  (interactive "DSelect directory: ")
  (ne/rg--search directory))

;;;###autoload
(defun ne/rg-other-project ()
  "Ripgrep in other project."
  (interactive)
  (let* ((default-directory
           (project-prompt-project-dir)))
    (ne/rg--search)))

;;;; Support functions

(defun ne/rg--search (&optional directory)
  (let ((consult-ripgrep-args ne/rg-command))
    (if directory
      (let ((default-directory directory))
        (call-interactively #'consult-ripgrep))
      (call-interactively #'consult-ripgrep))))

(provide 'ne-rg)

;;; ne/rg.el ends here
