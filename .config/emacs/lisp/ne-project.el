;;; ne-project.el --- Project -*- lexical-binding: t -*-

;;; Commentary:
;; This package provides project commands.

;;; Code:

;;;; Requirements

(require 'project)

;;;; Variables

(defcustom ne/project-compile-commands-list nil
  "List of compile commands."
  :type '(repeat string)
  :safe 'listp)

;;;; Commands

;;;###autoload
(defun ne/project-compile (command)
  "Run compile in project root."
  (interactive
   (list
    (pcase ne/project-compile-commands-list
      ((and (pred stringp) command) command)
      ((and (pred null)) compile-command)
      ((and (pred listp) commands)
       (completing-read "Select compile command: "
                        commands
                        nil t nil nil)))))
  (require 'dtache-compile)
  (let ((default-directory (ne/util-project-root)))
    (dtache-compile command)))

;;;###autoload
(defun ne/project-add (directory)
  "Add DIRECTORY to list of projects."
  (interactive "DSelect directory: ")
  (let ((default-directory directory))
    (project-remember-project (project-current))))

;;;###autoload
(defun ne/project-remove (project)
  "Remove PROJECT."
  (interactive
   (list (project-prompt-project-dir)))
  (project-remove-known-project project))

;;;###autoload
(defun ne/project-remove-missing-projects ()
  "Remove all missing projects."
  (interactive)
  (seq-map
   (lambda (project)
     (unless (file-exists-p (car project))
       (project--remove-from-project-list
        (car project) (format "Remove missing project: %s" (car project)))))
   project--list))

;;;###autoload
(defun ne/project-switch (project)
  "Switch to PROJECT."
  (interactive
   (list (project-prompt-project-dir)))
  (magit-status project))

;;;###autoload
(defun ne/project-recentf (project)
  "Recent files in PROJECT."
  (interactive
   (list (ne/util-project-root)))
  (let ((project-files
         (seq-filter (lambda (file)
                       (string-prefix-p project file))
                     recentf-list)))
    (find-file
     (completing-read "Find file: " project-files))))

(provide 'ne-project)

;;; ne/project.el ends here
