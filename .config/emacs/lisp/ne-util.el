;;; ne-util.el --- Util -*- lexical-binding: t -*-

;;; Commentary:

;; This package provides general utility commands.

;;; Code:

;;;; Requirements

(require 'tab-bar)

;;;; Macros

(defmacro ne/util-open-tab (name &rest body)
  "Open or switch to a tab with NAME and evaluate BODY."
  ;; Switch to tab if open. Otherwise creates it.
  `(let ((current-tab (alist-get 'name (tab-bar--current-tab)))
         (tabs
          (mapcar (lambda (tab)
                    (alist-get 'name tab))
                  (tab-bar-tabs))))
     (if (string= current-tab ,name)
         ,@body
       (if (member ,name tabs)
           (progn
             (tab-bar-switch-to-tab ,name)
             (when current-prefix-arg
               ,@body))
         (tab-bar-new-tab)
         (tab-bar-rename-tab ,name)
         ,@body))))

;;;; Commands

;;;###autoload
(defun ne/util-open-scratchpad ()
  "Open scratch buffer."
  (interactive)
  (pop-to-buffer "*scratch*"))

;;;###autoload
(defun ne/util-yank-buffer-filename (filename)
  "Copy the current buffer's path to the kill ring."
  (interactive
   (list (or buffer-file-name (bound-and-true-p list-buffers-directory))))
  (if filename
      (message (kill-new (abbreviate-file-name filename)))
    (message "Couldn't associate buffer with a file.")))

;;;###autoload
(defun ne/util-sudo-find-file (file-name)
  "Like `find-file', but opens the file as root."
  (interactive "FSudo Find File: ")
  (if (not (file-remote-p default-directory))
      (find-file (concat "/sudoedit::" (expand-file-name file-name)))
    (find-file (format "/%s:%s|sudo::%s"
                       (file-remote-p default-directory 'method)
                       (file-remote-p default-directory 'host)
                       (file-remote-p file-name 'localname)))))

;;;###autoload
(defun ne/util-remote-shell (host)
  "Open a remote shell on HOST."
  (interactive
   (list (ne/util-select-remote-host)))
  (let* ((default-directory (format "/ssh:%s:" host)))
    (shell (get-buffer-create (format "*%s*" host)))))

;;;###autoload
(defun ne/util-remote-find-file (host)
  "Use `find-file' on a remote HOST."
  (interactive
   (list (ne/util-select-remote-host)))
  (let* ((default-directory (format "/ssh:%s:" host)))
    (call-interactively #'find-file)))

;;;;; Functions

(defun ne/util-pop-to-buffer (orig-fun &rest args)
  "Advice `orig-fun' to use `pop-to-buffer'."
  (cl-letf (((symbol-function #'pop-to-buffer-same-window) #'pop-to-buffer)
            ((symbol-function #'switch-to-buffer) #'pop-to-buffer))
    (apply orig-fun args)))

(defun ne/util-find-pass (regexp)
  "Find `pass' entry containing REGEXP."
  (let* ((pass-store "~/.password-store")
         (pass-files (directory-files-recursively pass-store ".*.gpg")))
    (thread-last pass-files
      (seq-filter (lambda (it)
                    (with-temp-buffer
                      (insert-file-contents it)
                      (goto-char (point-min))
                      (re-search-forward regexp nil t)))))))

(defun ne/util-select-remote-host ()
  "Select and return a remote host."
  (let ((re (rx (: bol (regexp "^Host\s") (group (regexp ".*")))))
        (ssh-config "~/.ssh/config")
        (exclude-patterns '("^\*$" "\.com$"))
        (hosts))
    (when (file-exists-p ssh-config)
      (with-temp-buffer
        (insert-file-contents ssh-config)
        (goto-char (point-min))
        (while (search-forward-regexp re nil t)
          (push (match-string 1) hosts))))
    (setq hosts (seq-remove (lambda (host)
                              (seq-find (lambda (pattern)
                                          (string-match-p pattern host))
                                        exclude-patterns))
                            hosts))
    (completing-read "Select host: " hosts nil t)))

(defun ne/util-project-root ()
  "Return project root if in project otherwise `default-directory'."
  (expand-file-name
    (pcase (project-current)
      (`(,car . ,cdr) cdr)
      (_ default-directory))))

(defun ne/util-guix-add-package ()
  "Return git commit message for a new package."
  (let ((package-file (car (magit-staged-files)))
        (package-name
         (with-temp-buffer
           (magit-git-wash #'magit-diff-wash-diffs
             "diff" "--staged")
           (goto-char (point-min))
           (when (re-search-forward "\\+(define-public \\(\\S-+\\)" nil 'noerror)
             (match-string-no-properties 1)))))
    (format "gnu: Add %s. \n\n\* %s \(%s\): New variable." package-name package-file package-name)))

(defun ne/util-guix-update-package ()
  "Return git commit message for an updated package."
  (let ((package-file (car (magit-staged-files)))
        (package-version  (with-temp-buffer
                            (magit-git-wash #'magit-diff-wash-diffs
                              "diff" "--staged")
                            (goto-char (point-min))
                            (search-forward "name" nil 'noerror)
                            (search-forward "+" nil 'noerror)   ; first change
                            (when (and (search-forward "version " nil 'noerror)
                                       (looking-at-p "\""))
                              (let ((end (save-excursion (search-forward "\")" nil 'noerror))))
                                (when end
                                  (forward-char)
                                  (buffer-substring-no-properties (point) (- end 2)))))))
        (package-name  (with-temp-buffer
                         (magit-git-wash #'magit-diff-wash-diffs
                           "diff" "--staged")
                         (goto-char (point-min))
                         (when (re-search-forward "^ (define-public \\(\\S-+\\)" nil 'noerror)
                           (match-string-no-properties 1)))))
    (format "gnu: %s: Update to %s. \n\n\* %s \(%s\): Update to %s."
            package-name package-version package-file package-name package-version)))

(provide 'ne-util)

;;; ne/util.el ends here
