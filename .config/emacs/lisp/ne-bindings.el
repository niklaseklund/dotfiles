;;; ne-bindings.el --- Bindings -*- lexical-binding: t -*-

;; Copyright (C) 2022 Niklas Eklund

;; Author: Niklas Eklund <niklas.eklund@posteo.net>

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides bindings.

;;; Code:

;;;;; Global keys

(global-set-key (kbd "C-x C-b") nil)
(global-set-key (kbd "C-h C-f") nil)
(global-set-key (kbd "C-;") #'embark-act)
(global-set-key (kbd "C-h C-p") #'find-library)
(global-set-key (kbd "C-6") #'meow-last-buffer)

;;;;; Local keys

(define-key minibuffer-local-map [remap kill-region] #'backward-kill-word)
(define-key minibuffer-inactive-mode-map [remap kill-region] #'backward-kill-word)
(define-key (current-global-map) [remap kill-buffer] #'kill-this-buffer)

;;;;; Leader keys

;;;;; File

(meow-leader-define-key '("f ." . ne/fd-directory)
                        '("f f" . ne/fd-project)
                        '("f d" . recentd-dired-change-directory)
                        '("f F" . ne/fd-other-directory)
                        '("f p" . ne/fd-other-project)
                        '("f y" . ne/util-yank-buffer-filename))
;;;;; Project

(meow-leader-define-key '("p a" . ne/project-add)
                        '("p c" . ne/project-compile)
                        '("p d" . ne/fd-switch-to-project-directory)
                        '("p p" . ne/project-switch)
                        '("p r" . ne/project-remove-missing-projects))

;;;;; Notes

(meow-leader-define-key '("n s" . ne/rg-roam-directory)
                        '("n n" . org-roam-node-find)
                        '("n d" . ne/org-roam-switch-directory)
                        '("n b" . org-roam-buffer-toggle))

;;;;; Open

(meow-leader-define-key '("o a" . ne/org-agenda)
                        '("o d" . dtache-open-session)
                        '("o e" . ne/elfeed)
                        '("o g" . guix-popup)
                        '("o m" . ne/notmuch)
                        '("o p" . popper-toggle-latest)
                        '("o P" . popper-cycle)
                        '("o s" . shell)
                        '("o v" . magit-status)
                        '("o x" . ne/util-open-scratchpad)
                        '("o -" . dired-jump))

;;;;; Search

(meow-leader-define-key '("s a" . consult-org-agenda)
                        '("s b" . consult-line)
                        '("s d" . ne/rg-directory)
                        '("s D" . ne/rg-other-directory)
                        '("s f" . consult-flymake)
                        '("s i" . imenu)
                        '("s k" . devdocs-lookup)
                        '("s l" . link-hint-open-link)
                        '("s m" . consult-mark)
                        '("s M" . consult-global-mark)
                        '("s o" . consult-outline)
                        '("s p" . ne/rg-project)
                        '("s P" . ne/rg-other-project))
;;;;; Toggle

(meow-leader-define-key '("t f" . flymake-mode)
                        '("t g" . gif-screencast-start-or-stop)
                        '("t k" . keycast-tab-bar-mode)
                        '("t p" . org-tree-slide-mode)
                        '("t s" . spell-fu-mode)
                        '("t z" . olivetti-mode))
;;;;; Tabs

(meow-leader-define-key '("TAB d" . tab-bar-close-tab)
                        '("TAB c" . tab-bar-new-tab-to)
                        '("TAB n" . tab-bar-switch-to-next-tab)
                        '("TAB p" . tab-bar-switch-to-prev-tab)
                        '("TAB r" . tab-bar-rename-tab)
                        '("TAB o" . tab-bar-switch-to-recent-tab)
                        '("TAB O" . tab-bar-close-other-tabs)
                        '("TAB TAB" . tab-bar-select-tab-by-name))

;;;;; Version Control

(meow-leader-define-key '("v b" . magit-blame-addition)
                        '("v d" . diff-hl-diff-goto-hunk)
                        '("v l" . magit-log-buffer-file)
                        '("v r" . diff-hl-revert-hunk)
                        '("v s" . magit-stage-file)
                        '("v t" . git-timemachine-toggle)
                        '("v v" . magit-dispatch)
                        '("v [" . diff-hl-previous-hunk)
                        '("v ]" . diff-hl-next-hunk))

;;;;; Window

(meow-leader-define-key '("w w" . ace-select-window)
                        '("w o" . other-window)
                        '("w m" . delete-other-windows)
                        '("w s" . split-window-below)
                        '("w c" . delete-window)
                        '("w v" . split-window-horizontally)
                        '("w h" . windmove-left)
                        '("w j" . windmove-down)
                        '("w k" . windmove-up)
                        '("w l" . windmove-right)
                        '("w u" . tab-bar-history-back)
                        '("w r" . tab-bar-history-forward))

;;;;; Next/Previous

(meow-leader-define-key '("] e" . next-error))

(meow-leader-define-key '("[ e" . previous-error))

;;;;; Top level

(meow-leader-define-key '("," . consult-buffer)
                        '("'" . vertico-repeat))

;;;; Major mode

;;;; Minor mode

(provide 'ne-bindings)

;;; ne-bindings.el ends here
