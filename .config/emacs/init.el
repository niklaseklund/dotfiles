;;; init.el --- NE init.el -*- lexical-binding: t -*-

;; Copyright (C) 2020-2021 Niklas Eklund

;; Author: Niklas Eklund <niklas.eklund@posteo.net>
;; URL: https://www.gitlab.com/dotfiles
;; Package-Requires: ((emacs "27.1"))

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides the NE Emacs configuration

;;; Code:

;;;; Pre-configuration
;;;;; Constants

(defconst ne/guix-profile "~/.guix-profile")
(defconst ne/guix-extra-profiles (getenv "GUIX_EXTRA_PROFILES"))
(defconst ne/user-dir user-emacs-directory)
(defconst ne/lisp-dir (expand-file-name "lisp" ne/user-dir))
(defconst ne/cache-dir (expand-file-name "emacs" "~/.cache"))

;;;;; Load paths

;; Add paths to ne packages
(add-to-list 'load-path (expand-file-name ne/user-dir))
(add-to-list 'load-path ne/lisp-dir)

;; Add paths to packages installed with Guix
(require 'seq)
(require 'subr-x)

(setq guix-profiles
      `(,ne/guix-profile
        ,(expand-file-name "emacs/emacs" ne/guix-extra-profiles)))

(thread-last guix-profiles
  (seq-remove #'null)
  (seq-map (lambda (profile)
             (expand-file-name "share/emacs/site-lisp" profile)))
  (seq-filter #'file-exists-p)
  (seq-map (lambda (profile)
             (add-to-list 'load-path profile)
             (load (expand-file-name "subdirs.el" profile)))))

;;;;; Use-package

;; Use-package is used for package declarations
(eval-when-compile
  (require 'use-package))

;;;; Core
;;;;; Emacs

(use-package emacs
  :config
  ;; Don't prompt for confirmation when we create a new file or buffer (assume the
  ;; user knows what they're doing).
  (setq confirm-nonexistent-file-or-buffer nil)

  (setq uniquify-buffer-name-style 'forward)

  ;; no beeping or blinking please
  (setq ring-bell-function #'ignore)
  (setq visible-bell nil)

  ;; Avoid the "loaded old bytecode instead of newer source" pitfall.
  (setq load-prefer-newer t)

  ;; These are disabled directly through their frame parameters, to avoid the
  ;; extra work their minor modes do, but we have to unset these variables
  ;; ourselves, otherwise users will have to cycle them twice to re-enable them.
  (setq menu-bar-mode nil
        tool-bar-mode nil
        scroll-bar-mode nil)

  (setq window-resize-pixelwise t
        frame-resize-pixelwise t)

  (setq inhibit-startup-message t
        inhibit-startup-echo-area-message user-login-name
        inhibit-default-init t
        initial-major-mode 'emacs-lisp-mode)

  (setq use-file-dialog nil)

  ;; Don't autosave files or create lock/history/backup files. We don't want
  ;; copies of potentially sensitive material floating around or polluting our
  ;; filesystem. We rely on git and our own good fortune instead. Fingers crossed!
  (setq auto-save-default nil
        create-lockfiles nil
        make-backup-files nil)

  ;; Make all "yes or no" prompts show "y or n" instead
  (setq read-answer-short t)
  (fset 'yes-or-no-p 'y-or-n-p)

  ;; Line numbers
  ;; Explicitly define a width to reduce computation
  (setq-default display-line-numbers-width 3)

  ;; Show absolute line numbers for narrowed regions makes it easier to tell the
  ;; buffer is narrowed, and where you are, exactly.
  (setq-default display-line-numbers-widen t)

  ;; Enable line numbers in most text-editing modes. We avoid
  ;; `global-display-line-numbers-mode' because there are many special and
  ;; temporary modes where we don't need/want them.
  (add-hook 'prog-mode-hook #'display-line-numbers-mode)
  (add-hook 'text-mode-hook #'display-line-numbers-mode)
  (add-hook 'conf-mode-hook #'display-line-numbers-mode)

  ;; Add prompt indicator to `completing-read-multiple'.
  (defun crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t)
  (setq resize-mini-windows 'grow-only)

  ;; Corfu
  (setq completion-cycle-threshold 3)
  (setq tab-always-indent 'complete)

  ;; Indentation
  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 4)

  ;; Enable disabled commands
  (put 'narrow-to-region 'disabled nil)
  (put 'upcase-region 'disabled nil)

  ;; Vertico
  (setq read-extended-command-predicate
        #'command-completion-default-include-p))

;;;;; Native compilation

(use-package comp
  :custom (native-comp-async-report-warnings-errors nil))

;;;;; Guix

(use-package guix-emacs
  :config
  (guix-emacs-autoload-packages))

(use-package guix
  :defer t
  :hook ((shell-mode . guix-prettify-mode)
         (proced-mode . guix-prettify-mode)
         (dired-mode . guix-prettify-mode)))

;;;;; Garbage collection

(use-package gcmh
  :config
  (gcmh-mode 1))

;;;;; Littering

(use-package no-littering
  :init
  (setq user-emacs-directory "~/.cache/emacs")
  (make-directory user-emacs-directory t)
  :config
  (setq custom-file
        (no-littering-expand-etc-file-name "custom.el")))

;;;; NE packages

(use-package ne-bindings
  :after meow)

(use-package ne-fd
  :commands (ne/fd-project
             ne/fd-other-project
             ne/fd-directory
             ne/fd-other-directory
             ne/fd-switch-to-project-directory))

(use-package ne-project
  :commands (ne/project-add
             ne/project-remove
             ne/project-switch
             ne/project-recentf))

(use-package ne-rg
  :commands (ne/rg-project
             ne/rg-other-project
             ne/rg-roam-directory
             ne/rg-directory
             ne/rg-other-directory))

(use-package ne-util)

;;;; Local

(let ((local-config (expand-file-name "~/.config/emacs/local.el")))
  (load local-config 'noerror 'nomessage))

;;;; Vertical Completion Interface

(use-package vertico
  :init
  (vertico-mode)
  :custom ((vertico-cycle t)
           (vertico-resize t)
           (vertico-count 15)
           ( vertico-group-format nil)))

(use-package vertico-quick
  :after vertico
  :bind
  (:map vertico-map
         (("C-'" . vertico-quick-insert)
          ("C-q" . vertico-quick-exit))))

(use-package vertico-repeat
  :after vertico
  :config
  (add-hook 'minibuffer-setup-hook #'vertico-repeat-save))

(use-package vertico-reverse
  :after vertico)

(use-package vertico-multiform
  :after vertico
  :custom ((vertico-multiform-commands
            '((consult-imenu buffer)
              (consult-ripgrep buffer)
              (ne/rg-project buffer)
              (t reverse))))
  :config
  (vertico-multiform-mode))

(use-package orderless
  :init
  (setq completion-styles '(orderless))
  (setq completion-category-defaults nil)
  (setq completion-category-overrides '((file (styles . (partial-completion)))))
  (setq orderless-component-separator #'orderless-escapable-split-on-space)
  :config

  (defun ne/flex-if-twiddle (pattern _index _total)
    (when (string-suffix-p "~" pattern)
      `(orderless-flex . ,(substring pattern 0 -1))))

  (defun ne/first-initialism (pattern index _total)
    (if (= index 0) 'orderless-initialism))

  (defun ne/without-if-bang (pattern _index _total)
    (cond
     ((equal "!" pattern)
      '(orderless-literal . ""))
     ((string-prefix-p "!" pattern)
      `(orderless-without-literal . ,(substring pattern 1)))))

  (setq orderless-matching-styles '(orderless-regexp)
        orderless-style-dispatchers '(ne/flex-if-twiddle
                                      ne/without-if-bang)))

(use-package marginalia
  :hook (after-init . marginalia-mode)
  :bind (:map minibuffer-local-map
              ("M-A" . marginalia-cycle)))

(use-package embark
  :init
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  (setq y-or-n-p-use-read-key t))

(use-package consult
  :bind (;; C-c bindings (mode-specific-map)
         ("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c k" . consult-kmacro)
         ;; Remaps
         ([remap comint-history-isearch-backward-regexp] . consult-history)
         ([remap previous-matching-history-element] . consult-history)
         ([remap apropos] . consult-apropos)
         ([remap imenu] . consult-imenu)
         ([remap switch-to-buffer] . consult-buffer)
         ([remap list-bookmarks] . consult-bookmark)
         ([remap outline] . consult-outline)
         ([remap yank-pop] . consult-yank-from-kill-ring)
         ([remap multi-occur] . consult-multi-occur)
         ([remap locate] . consult-locate)
         ;; C-x bindings (ctl-x-map)
         ("C-x M-:" . consult-complex-command)
         ("C-x b" . consult-buffer)
         ("C-x 4 b" . consult-buffer-other-window)
         ("C-x 5 b" . consult-buffer-other-frame)
         ("C-x r b" . consult-bookmark)
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)
         ("<help> a" . consult-apropos)
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)
         ("M-g g" . consult-goto-line)
         ("M-g M-g" . consult-goto-line)
         ("M-g o" . consult-outline)
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings (search-map)
         ("M-s d" . consult-find)
         ("M-s D" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s m" . consult-multi-occur)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi))           ;; needed by consult-line to detect isearch
  :init
  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Optionally replace `completing-read-multiple' with an enhanced version.
  (advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  :config
  (consult-customize
   consult-theme
   :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-recent-file consult--source-project-recent-file consult--source-bookmark
   :preview-key (kbd "M-."))

  (setq consult-narrow-key "<")

  (setq consult-project-root-function
        (lambda ()
          (when-let (project (project-current))
            (car (project-roots project))))))

(use-package consult-eglot
  :after (consult eglot))

(use-package consult-dir
  :bind (("C-x C-d" . consult-dir)
         :map vertico-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file))
  :config
  (defvar consult-dir--source-recentd
    `(:name "Recentd dirs"
            :narrow ?r
            :category file
            :face consult-file
            :history file-name-history
            :enabled ,(lambda () recentd-mode)
            :items ,(lambda () recentd-list))
    "Recentd directory source for `consult-dir--pick'.")
  (setq consult-dir-sources
        '(consult-dir--source-bookmark
          consult-dir--source-default
          consult-dir--source-project
          consult-dir--source-recentd)))

(use-package consult-vertico
  :after (vertico consult))

(use-package embark-consult
  :after (embark consult))

;;;; UI
;;;;; Theme

(use-package modus-themes
  :custom ((modus-themes-mixed-fonts t)
           (modus-themes-headings '((1 . (variable-pitch 1.5))
                                    (2 . (variable-pitch 1.3))
                                    (3 . (variable-pitch 1.1))
                                    (t . (variable-pitch))))
           (modus-themes-paren-match '(intense bold))
           (modus-themes-syntax nil)
           (modus-themes-subtle-line-numbers t)
           (modus-themes-org-blocks 'gray-background)
           (modus-themes-mode-line '(borderless))
           (modus-themes-completions 'opinionated)
           (modus-themes-completions
            '((matches . (extrabold intense))
              (selection . (extrabold intense))
              (popup . (extrabold intense))))
           (modus-themes-region '(bg-only))
           (modus-themes-slanted-constructs t)
           (modus-themes-bold-constructs t)
           (modus-themes-diffs 'desaturated)))

(use-package modus-operandi-theme
  :when (string= (getenv "THEME") "light")
  :after modus-themes
  :config
  (load-theme 'modus-operandi t))

(use-package modus-vivendi-theme
  :when (string= (getenv "THEME") "dark")
  :after modus-themes
  :config
  (load-theme 'modus-vivendi t))

;;;;; Modeline

(use-package nano-modeline
  :hook (after-init . nano-modeline)
  :custom (nano-modeline-position 'bottom)
  :config
  (nano-modeline-mode))

;;;;; Window layout

(use-package popper
  :custom ((popper-reference-buffers
            '("\\*shell.*\\*"
              "\\*eshell.*\\*"
              "*Python*"
              "*ielm*"
              helpful-mode))
           (popper-display-control nil)
           (popper-mode-line nil))
  :init
  (popper-mode +1))

(use-package shackle
  :config
  (shackle-mode 1)
  (add-to-list 'shackle-rules '("*Messages*" :align below :size 0.33 :select t))
  (add-to-list 'shackle-rules '("*scratch*" :align below :size 0.4 :select t))
  (add-to-list 'shackle-rules '("*Python*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("*ielm*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("* Guile REPL *" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("*quickrun*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("*transmission*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("*Async Shell Command*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("*Dtache Shell Command*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("vterm" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("*shell*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("*eshell*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("vterm" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("*devdocs*" :align below :size 0.33 :select nil :popup t))
  (add-to-list 'shackle-rules '("*diff-hl*" :align below :size 0.4 :select t :popup t))
  (add-to-list 'shackle-rules '("*Proced*" :align below :size 0.4 :select t :popup t))
  (add-to-list 'shackle-rules '("*elfeed-entry*" :align below :size 0.8 :select t :popup t))
  (add-to-list 'shackle-rules '("*Bluetooth*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("\*docker-containers\*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("\*org-roam\*" :align right :size 0.33 :select nil :popup t))
  (add-to-list 'shackle-rules '("*Process List*" :align below :size 0.33 :select t :popup t))
  (add-to-list 'shackle-rules '("*Password-Store*" :align left :size 0.25 :select t :popup t))
  (add-to-list 'shackle-rules '("\*wclock\*" :align below :size 0.33 :select t :popup t)))

(use-package burly
  :commands (burly-bookmark-frames
             burly-bookmark-windows
             burly-open-bookmark))

;;;;; Fonts

(set-face-attribute 'default nil :font "Iosevka Term-16:weight=medium:style=medium")
(set-face-attribute 'fixed-pitch nil :font "Iosevka Term-16:weight=medium:style=medium")
(set-face-attribute 'variable-pitch nil :font "Roboto-16:weight=medium:style=medium")

;;;; Completion

(use-package corfu
  :custom ((corfu-count 10)
           (corfu-auto nil)
           (corfu-auto-prefix 3)
           (corfu-auto-delay 0.2)
           (corfu-quit-no-match t)
           (corfu-separator ?\s)
           (corfu-cycle t))
  :init
  (corfu-global-mode)
  :config
  (defun ne/corfu-auto-enable ()
    "Enable `corfu' auto completion in file."
    (add-hook 'post-command-hook #'corfu--auto-post-command nil 'local))
  (dolist (mode '(python-mode-hook c++-mode-hook emacs-lisp-mode-hook))
    (add-hook mode #'ne/corfu-auto-enable))

  (defun ne/corfu-enable-always-in-minibuffer ()
    "Enable Corfu in the minibuffer if Vertico/Mct are not active."
    (unless (or (bound-and-true-p mct--active)
                (bound-and-true-p vertico--input))
      (corfu-mode 1)))
  (add-hook 'minibuffer-setup-hook #'ne/corfu-enable-always-in-minibuffer 1))

(use-package cape
  :after minibuffer
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-keyword))

(use-package kind-icon
  :after corfu
  :custom
  ((kind-icon-default-face 'corfu-default) ; to compute blended backgrounds correctly
   (kind-icon-use-icons t))
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

;;;; Definitions

(use-package xref
  :load-path "~/.guix-extra-profiles/emacs/emacs/share/emacs/site-lisp/xref-1.4.0/"
  :config
  (remove-hook 'xref-backend-functions #'etags--xref-backend))

(use-package imenu
  :defer t
  :custom (imenu-max-item-length 90))

;;;; Search

(use-package grep
  :defer t)

(use-package wgrep
  :commands wgrep-change-to-wgrep-mode
  :custom (wgrep-auto-save-buffer t))

(use-package replace
  :defer t)

(use-package anzu
  :bind (([remap query-replace] . anzu-query-replace)
         ([remap query-replace-regexp] . anzu-query-replace-regexp))
  :config
  (global-anzu-mode +1))

;;;; Edit
;;;;; Structural editing

(use-package puni
  :hook ((lisp-mode . puni-mode)
         (emacs-lisp-mode . puni-mode)
         (scheme-mode . puni-mode))
  :bind (:map puni-mode-map
              (("C-)" . puni-slurp-forward)
               ("C-(" . puni-slurp-backward)
               ("C-}" . puni-barf-forward)
               ("C-{" . puni-barf-backward)
               ("M-S-s" . puni-split)
               ("M-s" . puni-splice)
               ("M-r" . puni-raise))))

(use-package electric
  :custom (electric-pair-inhibit-predicate
           (defun ne/electric-pair-inhibit-p (_)
             "`electric-pair-mode' is disabled when this function returns non nil."
             (member major-mode '(org-mode))))
  :config
  (electric-pair-mode))

(use-package paren
  :hook (after-init . show-paren-mode)
  :custom (show-paren-delay 0)
  :config
  (add-hook 'dired-mode-hook
            (defun ne/disable-show-paren ()
              (setq-local show-paren-mode nil))))

;;;; Navigation
;;;;; Windows

(use-package ace-window
  :defer t
  :custom (aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))

;;;;; Buffer

(use-package avy
  :defer t)

(use-package outline
  :hook ((emacs-lisp-mode . outline-minor-mode)
         (lisp-mode . outline-minor-mode)))

;;;; Layout

(use-package windmove
  :defer t)

(use-package tab-bar
  :hook ((after-init . tab-bar-mode)
         (after-init . tab-bar-history-mode)))

;;;; Utilities
;;;;; Authentication

(use-package auth-source
  :custom (auth-sources
           `(,(no-littering-expand-etc-file-name "authinfo.gpg"))))

(use-package auth-source-pass
  :config
  (auth-source-pass-enable))

(use-package pass
  :defer t
  :custom ((pass-username-field "user")
           (pass-show-keybindings nil)
           (password-store-password-length 20))
  :config

  (defun ne/pass-generate (entry &optional password-length)
    "Generate a new password for ENTRY with PASSWORD-LENGTH. Default PASSWORD-LENGTH is `password-store-password-length'."
    (interactive (list (read-string "Password entry: ")
                       (when current-prefix-arg
                         (abs (prefix-numeric-value current-prefix-arg)))))
    (let ((password-length (or password-length password-store-password-length))
          (no-symbols t))
      (password-store--run-generate entry password-length t no-symbols)
       nil))

  (advice-add 'password-store-generate :override #'ne/pass-generate))

;;;;; Customization

(use-package loaddefs
  :config
  (load custom-file 'noerror 'nomessage))

;;;;; Screencast

(use-package gif-screencast
  :commands gif-screencast-start-or-stop
  :defer t)

(use-package keycast
  :defer t
  :config
  ;; Replace input
  (dolist (input '(self-insert-command
                   org-self-insert-command))
    (add-to-list 'keycast-substitute-alist `(,input "." "Typing…")))

  (dolist (event '(mouse-event-p
                   mouse-movement-p
                   mwheel-scroll))
    (add-to-list 'keycast-substitute-alist `(,event nil))))

;;;;; Multimedia

(use-package emms
  :defer t
  :config
  (require 'emms-setup)
  (emms-all)
  (emms-default-players))

;;;;; Bluetooth

(use-package bluetooth
  :defer t
  :config
  (advice-add 'bluetooth-list-devices :around #'ne/util-pop-to-buffer))

;;;;; Transmission

(use-package transmission
  :defer t
  :custom ((transmission-refresh-modes '(transmission-mode
                                         transmission-files-mode
                                         transmission-info-mode
                                         transmission-peers-mode))
           (transmission-refresh-interval 1))
  :config
  (defun ne/transmission-start-daemon ()
    "Start the transmission daemon.
    The function is intended to be used to advice the transmission command. Ensuring
    that the daemon always runs when needed."
    (unless (with-temp-buffer
              (apply #'call-process '("herd" nil t nil "status" "transmission"))
              (goto-char (point-min))
              (search-forward-regexp "It is started" nil t))
      (apply #'call-process '("herd" nil nil nil "start" "transmission"))
      (sleep-for 0 200)))

  (advice-add 'transmission :before #'ne/transmission-start-daemon))

;;;;; IRC client

(use-package erc
  :commands ne/irc
  :custom ((erc-prompt-for-password nil))
  :config
  (setq erc-autojoin-channels-alist '(("irc.libera.chat" . ("#emacs" "#guix"))))

  (defun ne/irc ()
    "Open IRC."
    (interactive)
    (erc :server "irc.libera.chat" :nick "oakgrove")))

;;;;; RSS reader

(use-package elfeed
  :defer t
  :custom ((elfeed-show-entry-switch #'pop-to-buffer)
           (elfeed-show-entry-delete #'kill-buffer-and-window)
           (elfeed-feeds
            '(("http://nullprogram.com/feed/" blog emacs)
              ("https://irreal.org/blog/?feed=rss2" blog emacs)
              ("https://cestlaz.github.io/rss.xml" blog emacs)
              ("http://www.howardism.org/index.xml" blog emacs)
              ("https://sachachua.com/blog/category/emacs/feed/" blog emacs)
              ("http://pragmaticemacs.com/feed/" blog emacs)
              ("https://www.masteringemacs.org/feed" blog emacs)
              ("https://oremacs.com/atom.xml" blog emacs)
              ("https://ambrevar.xyz/atom.xml" blog emacs)
              ("https://protesilaos.com/codelog.xml" blog emacs)
              ("https://babbagefiles.xyz/atom.xml" blog emacs common-lisp stumpwm)
              ("https://medium.com/feed/@steve.yegge" blog)
              ("https://emacsredux.com/atom.xml" blog emacs)
              ("https://200ok.ch/atom.xml" blog emacs)
              ("https://emacsninja.com/feed.atom" blog emacs)
              ("https://karthinks.com/index.html" blog emacs)
              ("https://guix.gnu.org/feeds/blog.atom" blog guix)
              ("https://nyxt.atlas.engineer/feed" blog common-lisp)
              ("https://www.youtube.com/feeds/videos.xml?channel_id=UC0uTPqBCFIpZxlz_Lv1tk_g" vlog emacs) ; Protesilaos
              ("https://www.youtube.com/feeds/videos.xml?channel_id=UCFk8kgNu_bqsRZewxMGqkzQ" vlog emacs) ; Emacs SF
              )))
  :bind (:map elfeed-search-mode-map
              (("l" . ne/elfeed-watch-link)))
  :commands (ne/elfeed)
  :config
  (advice-add #'elfeed-kill-buffer :override #'kill-buffer-and-window)
  (advice-add #'elfeed-search-quit-window :after
              (defun ne/elfeed-search-quit ()
                "Remove `elfeed' tab."
                (tab-bar-close-tab)))

  (defun ne/elfeed ()
    "Open `elfeed' in a dedicated tab."
    (interactive)
    (ne/util-open-tab "rss" (elfeed)))

  (defun ne/elfeed-watch-link (&optional use-generic-p)
    "Watch link in feed(s)."
    (interactive "P")
    (require 'emms)
    (with-current-buffer (elfeed-search-buffer)
      (let ((entries (elfeed-search-selected)))
        (cl-loop for entry in entries
                 do (elfeed-untag entry 'unread)
                 when (elfeed-entry-link entry)
                 do (emms-play-url it))
        (mapc #'elfeed-search-update-entry entries)
        (unless (use-region-p) (forward-line)))))

  (setq elfeed-search-filter "@2-week-ago"))

;;;;; Notifications

(use-package alert
  :defer t
  :custom (alert-default-style 'libnotify))

;;;;; Process manager

(use-package proced
  :defer t)

;;;;; PDF reader

(use-package pdf-tools
  :mode ("\\.[pP][dD][fF]\\'" . pdf-view-mode)
  :magic ("%PDF" . pdf-view-mode)
  :custom ((pdf-view-display-size 'fit-page)
           (pdf-view-use-scaling t)
           (pdf-view-use-imagemagick nil))
  :config
  (add-hook 'pdf-view-mode-hook
            (defun ne/pdf-view-disable-blink-cursor ()
              (blink-cursor-mode -1))))

;;;;; Regular expressions

(use-package re-builder
  :defer t
  :custom (reb-re-syntax 'rx))

;;;;; Calendar

(use-package calendar
  :defer t
  :custom ((calendar-week-start-day 1)
           (calendar-intermonth-text
            '(propertize
              (format "%2d"
                      (car
                       (calendar-iso-from-absolute
                        (calendar-absolute-from-gregorian (list month day year)))))
              'font-lock-face 'font-lock-function-name-face))))

;;;;; Pulse

(use-package goggles
  :hook ((prog-mode text-mode) . goggles-mode)
  :config
  (setq-default goggles-pulse t))

(use-package pulse
  :commands ne/pulse-line
  :config
  (defun ne/pulse-current-line (&rest _)
    "Pulse the current line."
    (pulse-momentary-highlight-one-line (point)))

  (defun ne/pulse-line ()
    "Pulse the current line."
    (interactive)
    (ne/pulse-current-line)))

;;;; Git

(use-package magit
  :custom ((transient-default-level 5 magit-diff-refine-hunk t)
           (magit-display-buffer-function 'magit-display-buffer-fullframe-status-v1))
  :config
  (add-hook 'magit-status-sections-hook #'magit-insert-modules t))

(use-package magit-todos
  :after magit
  :custom (magit-todos-exclude-globs '("*"))
  :config
  (setq magit-todos-rg-extra-args '("--hidden"))
  (magit-todos-mode))

(use-package forge
  :after magit)

(use-package git-timemachine
  :defer t)

(use-package vc
  :defer t
  :custom (vc-follow-symlinks t "Follow symobolic links without asking."))

(use-package diff-hl
  :config
  (global-diff-hl-mode)
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))

(use-package ediff
  :defer t
  :custom ((ediff-diff-options "-w")
           (ediff-split-window-function #'split-window-horizontally)
           (ediff-window-setup-function #'ediff-setup-windows-plain))
  :config
  (defvar ne/ediff-window-conf nil "Saved window configuration.")

  (defun ne/ediff-save-window-conf ()
    "Save the current window configuration."
    (setq ne/ediff-window-conf (current-window-configuration)))

  (defun ne/ediff-restore-window-conf ()
    "Restore the window configuration prior to entering `ediff'."
    (when (window-configuration-p ne/ediff-window-conf)
      (set-window-configuration ne/ediff-window-conf)))

  (add-hook 'ediff-before-setup-hook #'ne/ediff-save-window-conf)
  (dolist (hook '(ediff-quit-hook ediff-suspended-hook))
    (add-hook hook #'ne/ediff-restore-window-conf)
    (add-hook hook #'ne/ediff-restore-window-conf)))

;;;; Programming
;;;;; LSP

(use-package eglot
  :hook ((c++-mode . eglot-ensure)
         (python-mode . eglot-ensure)))

;;;;; Debuggers

(use-package gud
  :defer t
  :bind (:map gud-mode-map
              (("C-x C-a C-S-q" . ne/gud-quit)))
  :config
  (defun ne/gud-quit ()
    "Quit `gud'."
    (interactive)
    (let ((tab-name (let-alist (funcall tab-bar-tabs-function) .current-tab.name)))
      (pcase tab-name
        ("gdb" (call-interactively #'ne/gdb-quit))
        ("pdb" (call-interactively #'ne/python-pdb-quit)))))

  (with-eval-after-load 'python
    (setq gud-pdb-command-name
          (concat python-shell-interpreter " -m pdb"))))

;;;;; Compilation

(use-package quickrun
  :defer t
  :config
  (quickrun-add-command "python"
    '((:command . "python3")
      (:compile-only . "pyflakes %s")
      (:description . "Run Python script"))
    :override t)
  (quickrun-set-default "c++" "c++/g++"))

;;;;; Linting

(use-package flymake
  :custom (flymake-fringe-indicator-position 'left-fringe)
  :config
  (add-to-list 'elisp-flymake-byte-compile-load-path load-path))

(use-package flymake-flycheck
  :after flymake
  :config
  (setq flymake-diagnostic-functions
        (flymake-flycheck-all-chained-diagnostic-functions)))

(use-package flycheck-package
  :init
  (defun flymake-package ()
    "Enable `flymake' linting for Emacs lisp."
    (setq-local flymake-diagnostic-functions
                `(,(flymake-flycheck-diagnostic-function-for 'emacs-lisp)
                  ,(flymake-flycheck-diagnostic-function-for 'emacs-lisp-checkdoc))))
  (add-hook 'emacs-lisp-mode-hook #'flymake-package)
  :config
  (setq flycheck-emacs-lisp-load-path 'inherit))

(use-package flymake-shellcheck
  :commands flymake-shellcheck-load
  :init
  (add-hook 'sh-mode-hook 'flymake-shellcheck-load))

(use-package flycheck-guile
  :init
  (defun flymake-guile ()
    "Enable `flymake' linting for guile."
    (setq-local flymake-diagnostic-functions
                `(,(flymake-flycheck-diagnostic-function-for 'guile))))

  (dolist (mode '(scheme-mode-hook geiser-mode-hook))
    (add-hook mode #'flymake-guile)))

;;;;; Formatters

(use-package whitespace-cleanup-mode
  :hook (prog-mode . whitespace-cleanup-mode))

;;;;; UI

(use-package prettify-symbols-mode
  :hook ((emacs-lisp-mode . prettify-symbols-mode)
         (lisp-mode . prettify-symbols-mode)))

(use-package hl-todo
  :hook (after-init . global-hl-todo-mode))

;;;; Languages
;;;;; C++

(use-package cc-mode
  :defer t
  :bind
  (:map c-mode-base-map
        (("TAB" . ne/cpp-indent-complete)))
  :config
  (defun ne/cpp-indent-complete ()
    (interactive)
    "Improve TAB behavior."
    (let (( p (point)))
      (c-indent-line-or-region)
      (when (= p (point))
        (call-interactively 'complete-symbol)))))

(use-package gdb-mi
  :commands (ne/gdb-open ne/gdb-conditional-breakpoint)
  :config
  (gdb-many-windows t)

  (defun ne/gdb-quit ()
    "Quit `gdb' and close tab."
    (interactive)
    (let ((gdb-modes '(gdb-locals-mode gdb-frames-mode gdb-breakpoints-mode))
          (gud-buffer
           (seq-find
            (lambda (buffer)
              (with-current-buffer buffer (eq major-mode 'gud-mode)))
            (buffer-list)))
          (kill-buffer-query-functions nil))
      (tab-bar-close-tab)
      (when gud-buffer
        (with-current-buffer gud-buffer
          (when-let* ((process (get-buffer-process (current-buffer)))
                      (active (eq 'run (process-status process))))
            (comint-send-eof))))
      (thread-last (buffer-list)
                   (seq-filter (lambda (buffer)
                                 (member (with-current-buffer buffer major-mode) gdb-modes)))
                   (seq-do #'kill-buffer))))

  (defun ne/gdb-open (&optional command)
    "Open `gdb' in a dedicated tab."
    (interactive
     (list "gdb -i=mi"))
    (let* ((default-directory
             (expand-file-name (ne/util-project-root))))
      (ne/util-open-tab "gdb" (gdb command))))

  (defun ne/gdb-conditional-breakpoint (&optional condition)
    "Set a conditional breakpoint for `gdb'."
    (interactive
     (list (read-string "Input condition: ")))
    (let ((location
           (concat
            (file-name-nondirectory (buffer-file-name))
            ":" (number-to-string (line-number-at-pos))))
          (gud-buffer (seq-find (lambda (it) (with-current-buffer it (eq major-mode 'gud-mode))) (buffer-list))))
      (with-current-buffer gud-buffer
        (insert (format "break %s if %s" location condition))
        (comint-send-input)))))

(use-package rmsbolt
  :defer t)

(use-package highlight-doxygen
  :custom (highlight-doxygen-commend-start-regexp "\\(/\\*\\(!\\|\\*[^*]\\)\\|///\\)" "Tweak the regexp to make emtpy line comments become fontified.")
  :custom-face (highlight-doxygen-comment ((t (:background nil))))
  :hook ((c++-mode . highlight-doxygen-mode)))

;;;;; Python

(use-package python
  :commands (ne/python-project-repl
             ne/python-module-name
             ne/python-pdb-open
             ne/python-pdb-quit)
  :config
  (advice-add 'python-shell-calculate-command :around
              (defun ne/python-shell-interpreter-a (orig-fun &rest args)
                "Recalculate `python-shell-interpreter' before calling ORIG-FUN with ARGS."
                (let* ((interpreters '(("python3" . "-i")
                                       ("python" . "-i")
                                       ("ipython" . "-i --simple-prompt --InteractiveShell.display_page=True")))
                       (selected-interpreter (thread-last interpreters
                                                          (seq-find (lambda (it) (executable-find (car it))))))
                       (python-shell-interpreter (car selected-interpreter))
                       (python-shell-interpreter-args (cdr selected-interpreter)))
                  (apply orig-fun args))))

  (defun ne/python-project-repl (project-root)
    "Open a `python' REPL from the PROJECT-ROOT."
    (interactive
     (list (ne/util-project-root)))
    (let ((default-directory project-root))
      (call-interactively #'run-python)))

  (defun ne/python-module-name (file)
    (interactive
     (list (buffer-file-name)))
    (let* ((file-without-ext (file-name-sans-extension file)))
      (kill-new
       (replace-regexp-in-string "/" "." (s-chop-prefix (ne/util-project-root) file-without-ext)))))

  (defun ne/python-pdb-open (&optional command)
    "Open `pdb' in a dedicated tab and run COMMAND."
    (interactive)
    (let* ((default-directory (ne/util-project-root))
           (pdb-command (concat python-shell-interpreter " -m pdb"))
           (command (or command (concat "-m " (ne/python-module-name (ne/fd-file `(".*\.py")))))))
      (ne/util-open-tab "pdb" (pdb (concat pdb-command " " command)))))

  (defun ne/python-pdb-quit ()
    "Quit `pdb'."
    (interactive)
    (tab-bar-close-tab)
    (let ((gud-buffer
           (seq-find
            (lambda (buffer)
              (with-current-buffer buffer (eq major-mode 'gud-mode)))
            (buffer-list))))
      (when gud-buffer
        (with-current-buffer gud-buffer
          (when-let* ((process (get-buffer-process (current-buffer)))
                      (active (eq 'run (process-status process))))
            (comint-send-eof)))))))

(use-package python-pytest
  :commands python-pytest-dispatch
  :custom (python-pytest-confirm nil))

(use-package python-black
  :after python
  :hook (python-mode . python-black-on-save-mode))

(use-package code-cells
  :hook (python-mode . code-cells-mode-maybe))

;;;;; Emacs Lisp

(use-package ielm
  :defer t)

(use-package elisp-demos
  :defer t
  :init
  (advice-add 'describe-function-1 :after #'elisp-demos-advice-describe-function-1)
  (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update))

;;;;; Common Lisp

(use-package sly
  :defer t
  :config
  (setq inferior-lisp-program (executable-find "sbcl")))

;;;;; Scheme

(use-package geiser
  :defer t
  :init
  (setq geiser-scheme-implementation 'guile))

;;;;; CMake

(use-package cmake-mode
  :defer t)

;;;;; Docker

(use-package docker
  :defer t)

;;;;; Json

(use-package json-mode
  :defer t)

;;;;; PlantUML

(use-package plantuml-mode
  :defer t)

;;;;; Shell scripts

(use-package sh-script
  :defer t)

;;;;; Bazel

(use-package bazel
  :defer t)

;;;; Documentation

(use-package info
  :defer t)

(use-package eldoc
  :custom (eldoc-echo-area-use-multiline-p nil))

(use-package devdocs
  :defer t)

(use-package helpful
  :bind
  (([remap describe-function] . helpful-callable)
   ([remap describe-command] . helpful-command)
   ([remap describe-variable] . helpful-variable)
   ([remap describe-key] . helpful-key)
   ([remap describe-symbol] . helpful-symbol)))

;;;; Org

;;;;; Core

(use-package org
  :defer t
  :custom ((org-todo-keywords  '((sequence
                                  "TODO(t)"   ; A task that needs doing & is ready to do
                                  "PROJ(p)"   ; A project, which usually contains other tasks
                                  "STRT(s)"   ; A task that is in progress
                                  "WAIT(w)"   ; Something external is holding up this task
                                  "HOLD(h)"   ; This task is paused/on hold because of me
                                  "DELEGATE(D)"                ; This task has been delegated
                                  "|"
                                  "DONE(d)"                    ; Task successfully completed
                                  "KILL(k)" ; Task was canceled, aborted or is no longer applicable ; Task was completed
                                  )))
           (org-file-apps '((auto-mode . emacs)
                            ("\\.mm\\'" . default)
                            ("\\.x?html?\\'" . default)
                            ("\\.pdf\\(::[0-9]+\\)?\\'" . ne/org-pdf-app)))
           (org-hide-emphasis-markers t)
           (org-ellipsis " ▼ ")
           (org-default-notes-file (concat (expand-file-name "~/sync/org") "/todo.org"))
           (org-directory "~/org")
           (org-babel-python-command (executable-find "python3"))
           (org-confirm-babel-evaluate nil))
  :config
  (defun ne/org-pdf-app (file-path link-without-schema)
    "Open FILE-PATH with `pdf-tools' and go to page in LINK-WITHOUT-SCHEMA.

     Always force pdf to be reread when exporting. This is done by
     temporarily altering the behavior of `yes-or-no-p'."
    (require 'pdf-tools)
    (cl-letf
        (((symbol-function 'yes-or-no-p) (lambda (_) t)))
      (let* ((page (if (not (string-match "\\.pdf::\\([0-9]+\\)\\'"
                                          link-without-schema))
                       1
                     (string-to-number (match-string 1 link-without-schema)))))
        (find-file-other-window file-path)
        (pdf-view-goto-page page))))

  (add-hook 'org-babel-after-execute-hook #'org-redisplay-inline-images)
  (advice-add 'org-babel-execute:python :around
              (defun ne/org-babel-python (orig-fun &rest args)
                "Update the `org-babel-python-command' before executing ORIG-FUN with ARGS."
                (let ((org-babel-python-command (executable-find "python3")))
                  (apply orig-fun args))))

  (add-hook 'org-mode-hook
            (defun ne/disable-line-numbers ()
              "Disable line-numbers."
              (display-line-numbers-mode -1)))

  (advice-add #'org-babel-execute-src-block :around
              (defun ne/org-babel-load (orig-fun &rest args)
                "Lazy-load babel languages."
                (pcase-let* ((`(,_ ,info ,_) args)
                             (language (intern (car info)))
                             (ob-lang (alist-get language '((elisp . emacs-lisp)
                                                            (python . python)
                                                            (sh . shell)
                                                            (plantuml . plantuml)
                                                            (cpp . C)))))
                  (unless (alist-get ob-lang org-babel-load-languages)
                    (add-to-list 'org-babel-load-languages `(,ob-lang . t))
                    (org-babel-do-load-languages 'org-babel-do-load-languages org-babel-load-languages))
                  (apply orig-fun args)))))

(use-package org-tempo
  :after org)

(use-package org-refile
  :after org
  :custom ((org-refile-targets '((nil :maxlevel . 5)))
           (org-outline-path-complete-in-steps nil)
           (org-refile-use-outline-path t)))

;;;;; UI

(use-package org-modern
  :load-path "~/opensource/org-modern"
  :hook (org-mode . org-modern-mode)
  :config
  (setq-default line-spacing 0.1))

;;;;; Agenda

(use-package org-agenda
  :commands ne/org-agenda
  :custom ((org-agenda-files (list "~/sync/org/todo.org"))
           (org-agenda-window-setup 'current-window))
  :config
  (defun ne/org-agenda ()
    "Open `org-agenda' in a dedicated tab."
    (interactive)
    (ne/util-open-tab "agenda" (org-agenda)))

  (add-to-list 'org-refile-targets '(org-agenda-files :maxlevel . 5))
  (advice-add #'org-agenda-quit :after
              (defun ne/org-agenda-quit ()
                "Remove `org-agenda' tab."
                (tab-bar-close-tab))))

;;;;; Presentations

(use-package org-re-reveal
  :after ox
  :custom ((org-re-reveal-revealjs-version "4")
           (org-re-reveal-root
            (format "file://%s" (expand-file-name "~/opensource/reveal.js")))))

(use-package org-tree-slide
  :defer t)

;;;;; Blogging

(use-package ox-hugo
  :after ox)

;;;;; Notes

(use-package org-roam
  :custom
  ((org-roam-directory (expand-file-name "~/roam-notes"))
   (org-roam-db-location (no-littering-expand-var-file-name "org/roam.db")))
  :commands (org-roam-node-find ne/org-roam-switch-directory)
  :config

  (defvar ne/org-roam-directories '("~/roam-notes") "List of `org-roam' directories.")
  (defun ne/org-roam-switch-directory ()
    "Switch `org-roam-directory'."
    (interactive)
    (let* ((directories (seq-filter #'file-exists-p ne/org-roam-directories))
           (directory (completing-read "Select org-roam directory: " directories nil t)))
      (setq org-roam-directory directory)
      (org-roam-db-sync t)))

  (org-roam-setup))

;;;;; Looks

(use-package org-indent-mode
  :hook (org-mode . org-indent-mode))

;;;; Writing

(use-package text-mode
  :bind (:map text-mode-map
              (("TAB" . completion-at-point)
               ("M-TAB" . indent-for-tab-command))))

(use-package markdown-mode
  :defer t)

(use-package olivetti
  :defer t
  :custom ((olivetti-body-width 0.5))
  :init
  (setq fill-column 100))

(use-package ispell
  :custom ((ispell-program-name "aspell")
           (ispell-personal-dictionary "~/.aspell.en.pws")
           (ispell-silently-savep t))
  :config
  (add-to-list 'ispell-aspell-dictionary-alist (ispell-aspell-find-dictionary "en_US"))
  (add-to-list 'ispell-aspell-dictionary-alist (ispell-aspell-find-dictionary "sv"))
  (ispell-set-spellchecker-params))

(use-package spell-fu
  :hook ((prog-mode . spell-fu-mode)
         (org-mode . spell-fu-mode))
  :config
  (add-hook 'git-commit-setup-hook #'spell-fu-mode)
  (add-hook 'org-mode-hook
            (defun ne/spell-fu-exclude-org-faces ()
              (setq spell-fu-faces-exclude
                    '(org-block-begin-line
                      org-block-end-line
                      org-code
                      org-date
                      org-drawer org-document-info-keyword
                      org-ellipsis
                      org-link
                      org-meta-line
                      org-properties
                      org-properties-value
                      org-special-keyword
                      org-src
                      org-tag
                      org-verbatim))))
  (add-hook 'spell-fu-mode-hook
            (defun ne/spell-fu-dictionaries ()
              (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "en_US"))
              (spell-fu-dictionary-add
               (spell-fu-get-personal-dictionary "en-personal" (expand-file-name "~/.aspell.en.pws"))))))

(use-package flyspell-correct
  :bind (([remap ispell-word] . flyspell-correct-at-point)))

(use-package synosaurus
  :defer t
  :custom (synosaurus-choose-method 'default))

(use-package visual-line-mode
  :hook (org-mode . visual-line-mode))

(use-package visual-fill-column
  :disabled
  :hook (org-mode . auto-fill-mode)
  :config
  (setq-default fill-column 80))

(use-package emojify
  :hook ((notmuch-search-mode . emojify-mode)
         (notmuch-show-mode . emojify-mode)
         (notmuch-message-mode . emojify-mode)))

;;;; Remote

(use-package tramp
  :custom ((tramp-default-method "ssh")))

;;;; Shell

(use-package comint
  :defer t
  :custom ((comint-input-ring-size 1000)
           (comint-password-prompt-regexp
            (concat comint-password-prompt-regexp "\\|^\\[dzdo\\]"))))

(add-to-list 'load-path "~/src/emacs-packages/dtache")
(use-package dtache
  :hook (after-init . dtache-setup)
  :bind (([remap async-shell-command] . dtache-shell-command))
  :custom ((dtache-db-directory (no-littering-expand-var-file-name "dtache"))
           (dtache-env (executable-find "dtache-env"))
           (dtache-notification-function #'ne/dtache-state-transition-alert-notification)
           (dtache-max-command-length 50)
           (dtache-tail-interval 1)
           (dtache-annotation-format
            '((:width 3 :function dtache--state-str :face dtache-state-face)
              (:width 3 :function dtache--status-str :face dtache-failure-face)
              (:width 10 :function dtache--host-str :face dtache-host-face)
              (:width 40 :function dtache--working-dir-str :face dtache-working-dir-face)
              (:width 10 :function dtache--duration-str :face dtache-duration-face)
              (:width 12 :function dtache--creation-str :face dtache-creation-face))))
  :config
  (setq dtache-metadata-annotators-alist '((branch . ne/dtache--session-git-branch)))

  ;; Embark
  (defvar embark-dtache-map (make-composed-keymap dtache-action-map embark-general-map))
  (add-to-list 'embark-keymap-alist '(dtache . embark-dtache-map))

  ;; Rsync
  (defun ne/dtache-dired-rsync (command _details)
    "Run COMMAND with `dtache'."
    (let ((dtache-local-session t)
          (dtache-session-origin 'rsync))
      (dtache-start-session command t)))

  (advice-add #'dired-rsync--do-run :override #'ne/dtache-dired-rsync)

  ;; Annotator
  (defun ne/dtache--session-git-branch ()
    "Return current git branch."
    (let ((git-directory (locate-dominating-file "." ".git")))
      (when git-directory
        (let ((args '("name-rev" "--name-only" "HEAD")))
          (with-temp-buffer
            (apply #'process-file `("git" nil t nil ,@args))
            (string-trim (buffer-string)))))))

  ;; Alert notification
  (defun ne/dtache-state-transition-alert-notification (session)
    "Send an `alert' notification when SESSION becomes inactive."
    (let ((status (car (dtache--session-status session)))
          (host (car (dtache--session-host session))))
      (alert (dtache--session-command session)
             :title (pcase status
                      ('success (format "Dtache finished [%s]" host))
                      ('failure (format "Dtache failed [%s]" host)))
             :severity (pcase status
                         ('success 'moderate)
                         ('failure 'high))))))

(use-package dtache-shell
  :after dtache
  :custom (dtache-shell-history-file "~/.bash_history")
  :config
  (dtache-shell-setup))

(use-package dtache-eshell
  :hook (eshell-mode . dtache-eshell-mode))

(use-package dtache-compile
  :after dtache
  :bind (([remap compile] . dtache-compile)
         ([remap recompile] . dtache-compile-recompile))
 :config
 (dtache-compile-setup))

(use-package dtache-org
  :after (dtache org)
  :config
  (dtache-org-setup))

(use-package dtache-consult
  :after dtache
  :custom ((dtache-consult-hidden-predicates
            '(ne/dtache-consult-rsync-p))
           (dtache-consult-sources
            '(dtache-consult--source-session
              dtache-consult--source-active-session
              dtache-consult--source-inactive-session
              dtache-consult--source-hidden-session
              dtache-consult--source-success-session
              dtache-consult--source-failure-session
              dtache-consult--source-local-session
              dtache-consult--source-remote-session
              dtache-consult--source-current-session)))
  :bind ([remap dtache-open-session] . dtache-consult-session)
  :config
  (defun ne/dtache-consult-rsync-p (session)
    "Return t if session is a rsync origin."
    (eq 'rsync (dtache--session-origin session))))

(use-package shell
  :defer t
  :init
  (setenv "PAGER" "cat")
  :custom-face (explicit-shell-file-name "/bin/bash"))

(use-package bash-completion
  :after shell
  :config
  (bash-completion-setup)

  (defun ne/shell-remote-completion-h ()
    "Remove completions that are slow on remote hosts."
    (when (file-remote-p default-directory)
      (dolist (completion '(bash-completion-dynamic-complete
                            shell-command-completion
                            pcomplete-completions-at-point))
        (remove-hook 'comint-dynamic-complete-functions completion t))))
  (add-hook 'shell-mode-hook #'ne/shell-remote-completion-h))

(use-package vterm
  :defer t
  :bind (:map vterm-mode-map
              ("<S-return>" . #'dtache-vterm-send-input)
              ("<C-return>" . #'dtache-vterm-attach)
              ("C-c C-d" . #'dtache-vterm-detach))
  :config

  (defun dtache-vterm-send-input (&optional detach)
    "Create a `dtache' session."
    (interactive "P")
    (vterm-send-C-a)
    (let* ((input (buffer-substring-no-properties (point) (vterm-end-of-line)))
           (dtache-session-origin 'vterm)
           (dtache-session-action
            '(:attach dtache-shell-command-attach-session
                      :view dtache-view-dwim
                      :run dtache-shell-command))
           (dtache-session-mode
            (if detach 'create 'create-and-attach)))
      (vterm-send-C-k)
      (process-send-string vterm--process (dtache-dtach-command input t))
      (vterm-send-C-e)
      (vterm-send-return)))

  (defun dtache-vterm-attach (session)
    "Attach to an active `dtache' session."
    (interactive
     (list
      (let* ((host-name (car (dtache--host)))
             (sessions
              (thread-last (dtache-get-sessions)
                           (seq-filter (lambda (it)
                                         (string= (car (dtache--session-host it)) host-name)))
                           (seq-filter (lambda (it) (eq 'active (dtache--determine-session-state it)))))))
        (dtache-completing-read sessions))))
    (let ((dtache-session-mode 'attach))
      (process-send-string vterm--process (dtache-dtach-command session t))
      (vterm-send-return)))

  (defun dtache-vterm-detach ()
    "Detach from a `dtache' session."
    (interactive)
    (process-send-string vterm--process dtache--dtach-detach-character)))

;;;; History

(use-package savehist
  :hook (after-init . savehist-mode)
  :custom ((savehist-save-minibuffer-history t)
           (savehist-autosave-interval nil)
           (savehist-additional-variables '(kill-ring
                                            search-ring
                                            regexp-search-ring
                                            recentd-history
                                            ne/fd-history)))
  :config
  (add-hook 'kill-emacs-hook
            (defun ne/unpropertize-kill-ring ()
              "Remove text properties from `kill-ring' for a smaller `savehist' file."
              (setq kill-ring
                    (cl-loop for item in kill-ring
                             if (stringp item)
                             collect (substring-no-properties item)
                             else if item collect it)))))

(use-package recentf
  :hook (after-init . recentf-mode)
  :custom (recentf-max-saved-items 50)
  :config
  (add-to-list 'recentf-exclude no-littering-var-directory)
  (add-to-list 'recentf-exclude no-littering-etc-directory))

(use-package recentd
  :hook (after-init . recentd-mode)
  :bind (:map shell-mode-map
              (("M-c" . recentd-shell-change-directory)))
  :custom (recentd-file (no-littering-expand-var-file-name "recentd-save.el")))

(use-package saveplace
  :hook (after-init . save-place-mode))

;;;; Dired

(use-package dired
  :defer t
  :bind (([remap dired-diff] . ne/dired-diff-files))
  :custom ((dired-dwim-target t)
           (dired-kill-when-opening-new-dired-buffer t)
           (dired-hide-details-hide-symlink-targets nil)
           (dired-recursive-copies 'always)
           (dired-recursive-deletes 'top))
  :config
  (defun ne/dired-diff-files ()
    "Diff marked files with `ediff'."
    (interactive)
    (require 'ediff)
    (let ((files (dired-get-marked-files)))
      (if (<= (length files) 2)
          (let ((file1 (car files))
                (file2 (if (cdr files)
                           (cadr files)
                         (read-file-name
                          "file: "
                          (dired-dwim-target-directory)))))
            (if (file-newer-than-file-p file1 file2)
                (ediff-files file2 file1)
              (ediff-files file1 file2)))
        (error "no more than 2 files should be marked")))))

(use-package wdired
  :after dired
  :custom ((wdired-allow-to-change-permissions t)
           (wdired-allow-to-redirect-links t)))

(use-package diredfl
  :after dired
  :config
  (diredfl-global-mode))

(use-package dired-rsync
  :after dired
  :bind
  (:map dired-mode-map
        (([remap dired-do-redisplay] . dired-rsync))))

(use-package dired-subtree
  :after dired
  :bind (:map dired-mode-map
              (("TAB" . dired-subtree-toggle))))

(use-package dired-narrow
  :after dired
  :bind (:map dired-mode-map
              ("N" . dired-narrow-regexp)))

;;;; Mail

(use-package notmuch
  :commands ne/notmuch
  :custom ((notmuch-fcc-dirs `((,(getenv "EMAIL"). "posteo/Sent -inbox +sent -unread +private")))
           (notmuch-search-oldest-first nil)
           (notmuch-saved-searches '((:name "inbox" :query "tag:inbox not tag:trash" :key "i")
                                     (:name "flagged" :query "tag:flagged" :key "f")
                                     (:name "sent" :query "tag:sent" :key "s")
                                     (:name "drafts" :query "tag:draft" :key "d"))))
  :init
  (setq send-mail-function 'sendmail-send-it
        message-kill-buffer-on-exit t
        message-send-mail-function 'message-send-mail-with-sendmail
        sendmail-program "msmtp")
  :config
  (defun ne/notmuch ()
    "Open `notmuch' in a dedicated tab."
    (interactive)
    (ne/util-open-tab "mail" (notmuch-hello)))

  (advice-add #'notmuch-bury-or-kill-this-buffer :around
              (defun ne/notmuch-quit (orig-fun &rest args)
                "delete tab when quitting `notmuch.'"
                (if (eq major-mode 'notmuch-hello-mode)
                    (progn
                      (apply orig-fun args)
                      (tab-bar-close-tab))
                  (apply orig-fun args)))))

(use-package ol-notmuch
  :after notmuch)

;;;; Environment

(use-package direnv
  :hook (after-init . direnv-mode)
  :config
  (add-to-list 'direnv-non-file-modes 'shell-mode))

;;;; Browse

(use-package link-hints
  :defer t)

(use-package browse-url
 :defer t)

(use-package browse-at-remote
  :commands (browse-at-remote
             ne/browse-remote-yank-url)
  :config
  (defun ne/browse-remote-yank-url ()
    "Yank remote url for code at point."
    (interactive)
    (require 'browse-at-remote)
    (kill-new
     (browse-at-remote-get-url))))

;;;; Snippets

(use-package tempel
  :bind (("M-+" . tempel-complete)
         ("M-*" . tempel-insert))
  :custom (tempel-file "~/.config/emacs/templates")
  :init
  (defun tempel-setup-capf ()
    "Add the Tempel Capf to `completion-at-point-functions'."
    (add-hook 'completion-at-point-functions #'tempel-expand -1 'local))
  (add-hook 'prog-mode-hook 'tempel-setup-capf)
  (add-hook 'text-mode-hook 'tempel-setup-capf))

;;;; Modal editing

(use-package modal-polyglot
  :disabled
  :after meow
  :custom ((modal-polyglot-enter-hook 'meow-insert-enter-hook)
           (modal-polyglot-exit-hook 'meow-insert-exit-hook)))

(use-package meow
  :init
  (meow-global-mode 1)
  :config
  (defun meow-setup ()
    (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
    (meow-motion-overwrite-define-key
     '("j" . meow-next)
     '("k" . meow-prev)
     '("<escape>" . ignore))
    (meow-leader-define-key
     ;; SPC j/k will run the original command in MOTION state.
     '("j" . "H-j")
     '("k" . "H-k")
     ;; Use SPC (0-9) for digit arguments.
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument)
     '("/" . meow-keypad-describe-key)
     '("?" . meow-cheatsheet))
    (meow-normal-define-key
     '("0" . meow-expand-0)
     '("9" . meow-expand-9)
     '("8" . meow-expand-8)
     '("7" . meow-expand-7)
     '("6" . meow-expand-6)
     '("5" . meow-expand-5)
     '("4" . meow-expand-4)
     '("3" . meow-expand-3)
     '("2" . meow-expand-2)
     '("1" . meow-expand-1)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("," . meow-inner-of-thing)
     '("." . meow-bounds-of-thing)
     '("[" . meow-beginning-of-thing)
     '("]" . meow-end-of-thing)
     '("a" . meow-append)
     '("A" . meow-open-below)
     '("b" . meow-back-word)
     '("B" . meow-back-symbol)
     '("c" . meow-change)
     '("d" . meow-delete)
     '("D" . meow-backward-delete)
     '("e" . meow-next-word)
     '("E" . meow-next-symbol)
     '("f" . meow-find)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("h" . meow-left)
     '("H" . meow-left-expand)
     '("i" . meow-insert)
     '("I" . meow-open-above)
     '("j" . meow-next)
     '("J" . meow-next-expand)
     '("k" . meow-prev)
     '("K" . meow-prev-expand)
     '("l" . meow-right)
     '("L" . meow-right-expand)
     '("m" . meow-join)
     '("n" . meow-search)
     '("o" . meow-block)
     '("O" . meow-to-block)
     '("p" . meow-yank)
     '("q" . meow-quit)
     '("Q" . meow-goto-line)
     '("r" . meow-replace)
     '("R" . meow-swap-grab)
     '("s" . meow-kill)
     '("t" . meow-till)
     '("u" . meow-undo)
     '("U" . meow-undo-in-selection)
     '("v" . meow-visit)
     '("w" . meow-mark-word)
     '("W" . meow-mark-symbol)
     '("x" . meow-line)
     '("X" . meow-goto-line)
     '("y" . meow-save)
     '("Y" . meow-sync-grab)
     '("z" . meow-pop-selection)
     '("'" . repeat)
     '("/" . avy-goto-char-timer)
     '("<escape>" . ignore)))
  (setq meow-cheatsheet-physical-layout meow-cheatsheet-physical-layout-ansi)
  (meow-setup)
  (meow-setup-indicator)
  (add-to-list 'meow-mode-state-list '(notmuch-hello-mode . motion))
  (add-to-list 'meow-mode-state-list '(pass-mode . motion)))

;;; init.el ends here
