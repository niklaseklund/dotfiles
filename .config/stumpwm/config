;; -*-lisp-*-
;;

(in-package :stumpwm)

;;; Slynk

(load (concat (getenv "HOME") "/.config/stumpwm/setup_slynk.lisp"))

;;; Inspirations

;; https://www.notabug.org/alezost/stumpwm-config

;;; Init

(setf *startup-message* NIL
      *suppress-abort-messages* t
      (getenv "GDK_CORE_DEVICE_EVENTS") "1" ;mousewheel scroll fix
      *shell-program* (getenv "SHELL"))

(defvar *message-filters* '("Group dumped")
  "Don't show the messages from winner-mode.")

(defun message (fmt &rest args)
  "Overwritten message function to allow filters"
  (let ((msg-string (apply 'format nil fmt args)))
    (unless (member msg-string *message-filters* :test #'string=)
      (echo-string (current-screen) msg-string))))

;;; Modules

;;;; Load

(set-module-dir
  (pathname-as-directory (concat (getenv "HOME") "/.guix-extra-profiles/stumpwm/stumpwm/share/common-lisp/sbcl")))

(mapc #'load-module '("swm-gaps"
                      "ttf-fonts"
                      "globalwindows"
                      "wifi"
                      "cpu"
                      "mem"))

;;;; Fonts

(require 'clx-truetype)
(setf xft:*font-dirs*
      (cons (concat (getenv "HOME") "/.guix-extra-profiles/font/font/share/fonts/")
            xft:*font-dirs*)) ; add font guix-profile to list of font directories
(setf xft:+font-cache-filename+
      (concat (getenv "HOME") "/.cache/stumpwm/font-cache.sexp")) ;default is a path into the /gnu/store which is not writable
(xft:cache-fonts)                       ; make a hash table of the system fonts which will be put in the +font-cache-filename+
(set-font (make-instance 'xft:font :family "Iosevka Term" :subfamily "Regular" :size 12))

;;;; Gaps

(setf swm-gaps:*head-gaps-size* 0) ;; Head gaps run along the 4 borders of the monitor(s)
(setf swm-gaps:*inner-gaps-size* 10) ;; Inner gaps run along all the 4 borders of a window
(setf swm-gaps:*outer-gaps-size* 10) ;; Outer gaps add more padding to the outermost borders of a window
(eval-command "toggle-gaps")

;;; Looks

(setf *message-window-gravity* :center
      *input-window-gravity* :center
      *window-border-style* :none
      *message-window-padding* 10
      *maxsize-border-width* 5
      *normal-border-width* 5
      *transient-border-width* 2
      stumpwm::*float-window-border* 2
      stumpwm::*float-window-title-height* 5
      *mouse-focus-policy* :click)

(set-normal-gravity :center)
(set-maxsize-gravity :center)
(set-transient-gravity :center)

(when (string= "dark" (getenv "THEME"))
  (set-fg-color "#eeeeee")
  (set-bg-color "#1C2028")
  (set-border-color "#232731")
  (set-focus-color "#3B4252")
  (set-unfocus-color "#232731")
  (set-win-bg-color "#22272F")
  (set-float-focus-color "#3B4252")
  (set-float-unfocus-color "#232731")

  (setf *colors* (list "#1C2028"        ; 0 black
                       "#BF616A"        ; 1 red
                       "#A3BE8C"        ; 2 green
                       "#EBCB8B"        ; 3 yellow
                       "#5E81AC"        ; 4 blue
                       "#B48EAD"        ; 5 magenta
                       "#8FBCBB"        ; 6 cyan
                       "#ECEFF4")))    ; 7 white

(when (string= "light" (getenv "THEME"))
  (set-fg-color "#1C2028")
  (set-bg-color "#eeeeee")
  (set-border-color "#232731")
  (set-focus-color "#3B4252")
  (set-unfocus-color "#232731")
  (set-win-bg-color "#22272F")
  (set-float-focus-color "#3B4252")
  (set-float-unfocus-color "#232731")

  (setf *colors* (list "#1C2028"        ; 0 black
                       "#BF616A"        ; 1 red
                       "#A3BE8C"        ; 2 green
                       "#EBCB8B"        ; 3 yellow
                       "#5E81AC"        ; 4 blue
                       "#B48EAD"        ; 5 magenta
                       "#8FBCBB"        ; 6 cyan
                       "#ECEFF4")))    ; 7 white


;;; Functions

(defun rofi (mode)
  (run-shell-command
   (concat "rofi -show " mode " -theme themes/" (getenv "THEME") ".rasi")))

;;; Commands

;; (defcommand scratchpad-term () ()
;;   (scratchpad:toggle-floating-scratchpad "term" "kitty"
;;                                          :initial-gravity :center
;;                                          :initial-width 1200
;;                                          :initial-height 800))

(defcommand rofi-run () () (rofi "run"))
(defcommand colon1 (&optional (initial "")) (:rest)
  (let ((cmd (read-one-line (current-screen) ": " :initial-input initial)))
    (when cmd
      (eval-command cmd t))))

(defcommand nyxt () ()
  "run nyxt"
  (run-or-raise "nyxt" '(:class "nyxt")))
(defcommand firefox () ()
  "run firefox"
  (run-or-raise "firefox" '(:class "Nightly")))
(defcommand spotify () ()
  "run spotify"
  (run-or-raise "spotify" '(:class "Spotify")))
(defcommand teams () ()
  "run teams"
  (run-or-raise "teams" '(:class "Microsoft Teams - Preview")))
(defcommand slack () ()
  "run slack"
  (run-or-raise "slack" '(:class "Slack")))
(defcommand emacs () ()
  "run emacs"
  (run-or-raise "emacsclient --create-frame" '(:class "Emacs")))

;;; Bindings

(load (concat (getenv "HOME") "/.config/stumpwm/setup_bindings.lisp"))

;;; Groups

(setf (group-name (car (screen-groups (current-screen)))) "MAIN")
(run-commands "gnewbg EXTRA" "gnewbg FLOAT")

;;; Window rules

;; TODO: How to make primary screen the first frame of choice?
(clear-window-placement-rules)

(define-frame-preference "CHAT"
    (nil nil t :class "Microsoft Teams - Preview")
  (nil nil t :class "Slack"))

(define-frame-preference "WEB"
  (nil nil t :class "Google-chrome"))

;;; Modeline

(load (concat (getenv "HOME") "/.config/stumpwm/setup_modeline.lisp"))

;;; Services

(run-shell-command "shepherd")

;;; Overrides

(defcommand quit-guix () ()
  "Termine shepherd services before exiting."
  (let ((services '("emacs" "vpn")))
    (mapc
     (lambda (service)
       (run-shell-command (concat "herd stop " service) t))
     services))
  (quit))
