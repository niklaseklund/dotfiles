;; -*-lisp-*-
;;

;;; Keymaps
;;;; Window

(defvar *my-window-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "s") "vsplit")
    (define-key m (kbd "v") "hsplit")
    (define-key m (kbd "w") "windowlist")
    (define-key m (kbd "c") "delete-window")
    (define-key m (kbd "m") "only")
    (define-key m (kbd "o") "fother")
    (define-key m (kbd "p") "global-pull-windowlist")
    (define-key m (kbd "=") "balance-frames")
    (define-key m (kbd "r") "iresize")
    (define-key m (kbd "h") "move-focus left")
    (define-key m (kbd "j") "move-focus down")
    (define-key m (kbd "k") "move-focus up")
    (define-key m (kbd "l") "move-focus right")
    (define-key m (kbd "H") "move-focus left")
    (define-key m (kbd "J") "move-window down")
    (define-key m (kbd "K") "move-window up")
    (define-key m (kbd "L") "move-window right")
    m))

(define-key *top-map* (kbd "s-w") '*my-window-bindings*)

;;;; Frame

(defvar *my-frame-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "o") "other-in-frame")
    (define-key m (kbd "f") "frame-windowlist")
    (define-key m (kbd "n") "next-in-frame")
    (define-key m (kbd "p") "prev-in-frame")
    m))

(define-key *top-map* (kbd "s-f") '*my-frame-bindings*)

;;;; Group

(defvar *my-group-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "TAB") "grouplist")
    (define-key m (kbd "c") "gnew")
    (define-key m (kbd "p") "gprev")
    (define-key m (kbd "n") "gnext")
    (define-key m (kbd "d") "gkill")
    (define-key m (kbd "g") "vgroups")
    (define-key m (kbd "l") "gother")
    (define-key m (kbd "r") "grename")
    (define-key m (kbd "o") "gother")
    (define-key m (kbd "m") "gmove")
    m))

(define-key *top-map* (kbd "s-g") '*my-group-bindings*)

;;;; Open

(defvar *my-open-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "b") "nyxt")
    (define-key m (kbd "f") "firefox")
    (define-key m (kbd "p") "exec rofi-pass")
    (define-key m (kbd "e") "emacs")
    (define-key m (kbd "E") "exec emacs")
    (define-key m (kbd "m") "spotify")
    (define-key m (kbd "s") "slack")
    (define-key m (kbd "t") "teams")
    m))

(define-key *top-map* (kbd "s-o") '*my-open-bindings*)

;;;; Toggle

(defvar *my-toggle-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "g") "toggle-gaps")
    (define-key m (kbd "s") "toggle-slynk")
    (define-key m (kbd "m") "mode-line")
    m))

(define-key *top-map* (kbd "s-t") '*my-toggle-bindings*)

;;;; Help

(defvar *my-help-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "f") "describe-function")
    (define-key m (kbd "v") "describe-variable")
    (define-key m (kbd "c") "commands")
    (define-key m (kbd "C") "describe-command")
    (define-key m (kbd "k") "describe-key")
    (define-key m (kbd "w") "list-window-properties")
    (define-key m (kbd "W") "where-is")
    m))

;;;; Kill

(defvar *my-kill-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "s") "quit-guix")
    m))

;;;; Leader

(define-key stumpwm:*top-map* (kbd "s-SPC") '*my-leader-bindings*)

(defvar *my-leader-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "h") '*my-help-bindings*)
    (define-key m (kbd "k") '*my-kill-bindings*)
    (define-key m (kbd ":") "colon")
    m))

;;;; Top level

;; Groups

(define-key *top-map* (kbd "s-1") "gselect 1")
(define-key *top-map* (kbd "s-2") "gselect 2")
(define-key *top-map* (kbd "s-3") "gselect 3")
(define-key *top-map* (kbd "s-4") "gselect 4")
(define-key *top-map* (kbd "s-5") "gselect 5")

;; Commands

(define-key *top-map* (kbd "s-;") "eval-line")
(define-key *top-map* (kbd "s-:") "colon")
(define-key *top-map* (kbd "s-&") "exec")
(define-key *top-map* (kbd "s-q") "send-raw-key")

;; Rofi

(define-key *top-map* (kbd "s-.") "rofi-run")
(define-key *top-map* (kbd "s-,") "global-windowlist")

;; Programs

(define-key *top-map* (kbd "s-RET") "exec kitty")

;; Light

(define-key *top-map* (kbd "XF86MonBrightnessDown") "exec light -U 5")
(define-key *top-map* (kbd "XF86MonBrightnessUp") "exec light -A 5")

;; Sound

(define-key *top-map* (kbd "XF86AudioRaiseVolume") "exec pactl set-sink-volume @DEFAULT_SINK@ +5%")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "exec pactl set-sink-volume @DEFAULT_SINK@ -5%")
(define-key *top-map* (kbd "XF86AudioMute") "exec pactl set-sink-mute @DEFAULT_SINK@ toggle")

;; Screenshots

(define-key *top-map* (kbd "SunPrint_Screen") "screenshot-window")
