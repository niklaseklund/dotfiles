;; -*-lisp-*-
;;

(require 'slynk)

(defparameter *port-number* 4010
  "My default port number for Slynk")

(defparameter *slynk-server-p* nil
  "Keep track of slynk server.")

(defcommand start-slynk () ()
  "Start slynk if it is not already running."
  (if *slynk-server-p*
      (message "Slynk server is already active.")
      (progn
        (slynk:create-server
         :style slynk:*communication-style*
         :dont-close t
         :port 4010)
        (setf *slynk-server-p* t)
        (message "Slynk server is now active."))))

(defcommand stop-slynk () ()
  "Stop slynk."
  (slynk:stop-server *port-number*)
  (setf *slynk-server-p* nil)
  (message "Stopping slynk server."))

(defcommand toggle-slynk () ()
  "Toggle slynk."
  (if *slynk-server-p*
      (run-commands "stop-slynk")
      (run-commands "start-slynk")))
