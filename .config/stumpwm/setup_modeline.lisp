;; modeline status
(defun get-slynk-status ()
  (if *slynk-server-p*
      (format nil "Slynk Port: ~a^n | " *port-number*)
      ""))

(defun ml-fmt-slynk-status (ml)
  (declare (ignore ml))
  (get-slynk-status))

(add-screen-mode-line-formatter #\S #'ml-fmt-slynk-status)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Modeline settings                                                       ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Apperance
(setf *mode-line-timeout* 1)
(setf *mode-line-border-width* 0)
(setf *mode-line-pad-x* 20)
(setf *mode-line-pad-y* 10)

;; Windows
(setf *window-format* "%m%n%s%20t")

;; Colors
(when (string= "dark" (getenv "THEME"))
  (setf *mode-line-background-color* "#000000"))
(when (string= "light" (getenv "THEME"))
  (setf *mode-line-background-color* "#eeeeee"))
(setf *mode-line-foreground-color* "#B48EAD")
;; (setf *mode-line-border-color* *bg-color*)

;; Time
(setf *time-modeline-string* "%a %b %e %Y %k:%M")

;; Battery
(defparameter *battery-percent* "")

(defun get-battery-status ()
 (let* ((batgetcap (run-shell-command "cat /sys/class/power_supply/BAT0/capacity | tr -d '\\r\\n'" t)))
   (setf *battery-percent* (format nil "Battery ~a% | " batgetcap))))

(defun battery-percentage (ml)
 (declare (ignore ml))
 *battery-percent*)

(run-with-timer 0 10 #'get-battery-status)
(add-screen-mode-line-formatter #\B #'battery-percentage) 

;; Keyboard layout
(defparameter *keyboard-layout* "")

(defun get-keyboard-layout ()
  (let* ((layout (string-trim '(#\Space #\Tab #\Newline) (run-shell-command "xkb-switch -p" t))))
    (setf *keyboard-layout* (format nil "Language ~a " layout))))

(run-with-timer 0 10 #'get-keyboard-layout)

(defun keyboard-layout (ml)
 (declare (ignore ml))
 *keyboard-layout*)

(add-screen-mode-line-formatter #\L #'keyboard-layout)

;; VPN
(defparameter *vpn-status* "")

(defun get-vpn-status ()
  (let* ((layout (string-trim '(#\Space #\Tab #\Newline)
                              (run-shell-command "nmcli connection show --active | awk '{print $3 }' | grep vpn" t))))
    (if (string-match "" layout)
        (setf *vpn-status* "")
        (setf *vpn-status* "vpn | "))))

(run-with-timer 0 10 #'get-vpn-status)

(defun vpn-status (ml)
 (declare (ignore ml))
 *vpn-status*)

(add-screen-mode-line-formatter #\V #'vpn-status)

;; Wifi
(setf wifi:*iwconfig-path* (string-trim '(#\Space #\Tab #\Newline) (run-shell-command "which iwconfig" t)))
(setf wifi:*wireless-device* "wlp3s0")
(setf wifi:*wifi-modeline-fmt* "%e %p")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setf *screen-mode-line-format*
      (list "[%g]^n "                 ; groups
            "%W"                        ; windows
            "^>"                        ; right align
            "%S"                        ; slynk status
            "%C | "                     ; cpu status
            "%M | "                     ; memory status
            "%L | "                     ; keyboard layout
            "%V"                        ; vpn
            "%I | "                     ; wifi
            "%B"                        ; battery percentage
            "%d"                        ; time/date
            ))

;; turn on the mode line
(if (not (head-mode-line (current-head)))
    (toggle-mode-line (current-screen) (current-head)))
