[colors]
accent-dark = #44333446
accent-light = #bbbbbb
background = ${colors.accent-dark}
background-tray = ${colors.accent-dark}
foreground = #FFFFFF
foreground-alt = ${colors.accent-light}
alert = #BF616A

[global/wm]
margin-top = 2
margin-bottom = 2

[bar/laptop]
monitor = eDP-1
override-redirect = true
width = 100%
height = 28
offset-x = 0
offset-y = 0

background = ${colors.background}
foreground = ${colors.foreground}

overline-size = 1
overline-color = ${colors.background}
underline-size = 2
underline-color = ${colors.background}

border-top-size = 0
border-top-color = ${colors.background}
border-bottom-size = 0
border-bottom-color = ${colors.background}

padding-left = 3
padding-right = 3
module-margin-left = 1
module-margin-right = 1

font-0 = "Source Code Pro:size=12;1"
font-1 = "FontAwesome:pixelsize=12;1"

; modules-left = ewmh alsa mail
; modules-center =
; modules-right = redshift cpu memory wlan battery vpn date

modules-left = ewmh
modules-center =
modules-right = cpu memory wlan keyboard battery date

; tray-position = left
; tray-padding = 1
; tray-background = ${colors.background-tray}

enable-ipc = true

[bar/main]
monitor = DP-2
override-redirect = true
width = 100%
height = 28
offset-x = 0
offset-y = 0

background = ${colors.background}
foreground = ${colors.foreground}

overline-size = 1
overline-color = ${colors.background}
underline-size = 2
underline-color = ${colors.background}

border-top-size = 0
border-top-color = ${colors.background}
border-bottom-size = 0
border-bottom-color = ${colors.background}

padding-left = 3
padding-right = 3
module-margin-left = 1
module-margin-right = 1

font-0 = "Source Code Pro:size=12;1"
font-1 = "FontAwesome:pixelsize=12;1"

; modules-left = ewmh alsa mail
; modules-center =
; modules-right = cpu memory wlan battery vpn date

modules-left = ewmh
modules-center =
modules-right = cpu memory wlan keyboard battery date

; tray-position = left
; tray-padding = 1
; tray-background = ${colors.background-tray}

enable-ipc = true

[module/ewmh]
type = internal/xworkspaces

enable-click = true
enable-scroll = false

icon-0 = MAIN;
icon-1 = WEB;
icon-2 = CHAT;
icon-3 = MEDIA;
icon-4 = FLOAT;
icon-5 = TEXT;
icon-default = 

label-active = "  %icon% %name% "
label-active-foreground = ${colors.foreground}
label-active-background = #882E3440
label-active-underline = ${colors.accent-light}

label-empty = "  %icon% %name% "
label-empty-foreground = ${colors.foreground-alt}

label-occupied = "  %icon% %name% "
label-occupied-foreground = ${colors.foreground}

[module/keyboard]
type = internal/xkeyboard

; List of indicators to ignore
blacklist-0 = num lock
blacklist-1 = scroll lock
blacklist-2 = caps lock

; Available tags:
;   <label-layout> (default)
;   <label-indicator> (default)
format = <label-layout> <label-indicator>
format-spacing = 0

; Available tokens:
;   %layout%
;   %name%
;   %number%
; Default: %layout%
label-layout = %name%

; Available tokens:
;   %name%
; Default: %name%
label-indicator-on = %name%
label-indicator-padding = 2

[module/redshift]
type = custom/script
interval = 5
format-prefix = "RDSFT "
format-prefix-foreground = ${colors.foreground-alt}
exec = ~/.config/polybar/modules/redshift.sh
click-left = ~/.config/polybar/modules/redshift-toggle.sh

[module/vpn]
type = custom/script
interval = 5
format-prefix = "VPN "
format-prefix-foreground = ${colors.foreground-alt}
exec = ~/.config/polybar/modules/vpn.sh

[module/mail]
type = custom/script
exec = ~/.config/polybar/modules/mail.sh
interval = 10

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = "CPU "
format-prefix-foreground = ${colors.foreground-alt}
label = %percentage%%

[module/memory]
type = internal/memory
interval = 3
format-prefix = "MEM "
format-prefix-foreground = ${colors.foreground-alt}
label = %percentage_used%%

[module/wlan]
type = internal/network
interface = wlp3s0
interval = 3

format-connected = "<label-connected> "
format-connected-prefix = "  "
format-connected-underline = ${colors.accent-light}

label-connected = %essid%
label-disconnected =  WLAN
label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 5
time = "%H:%M"
date = "%a %d %b"
label = "%time%, %date%"
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-background = ${colors.background}
label-background = ${colors.background}

[module/alsa]
type = internal/alsa

format-volume = <ramp-volume> <label-volume>
label-volume = %percentage%%

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 
ramp-volume-foreground = ${colors.foreground-alt}

label-muted =  MUTED
format-muted-foreground = ${colors.foreground-alt}

[module/battery]
type = internal/battery
battery = BAT0
adapter = ADP1
full-at = 98

format-charging = <animation-charging> <label-charging>
format-discharging = <ramp-capacity> <label-discharging>

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-foreground = ${colors.alert}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-foreground = ${colors.foreground}
animation-charging-framerate = 750
