# Bash initialization for interactive non-login shells and
# for remote shells (info "(bash) Bash Startup Files").

# Export 'SHELL' to child processes.  Programs such as 'screen'
# honor it and otherwise use /bin/sh.
export SHELL

if [[ $- != *i* ]]
then
    # We are being invoked from a non-interactive shell.  If this
    # is an SSH session (as in "ssh host command"), source
    # /etc/profile so we get PATH and other essential variables.
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile

    # Don't do anything else.
    return
fi

# Configuration
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=50000
export HISTFILESIZE=50000
export HISTIGNORE='rm *:dtach*'
shopt -s histappend

# Source the system-wide file.
[ -f /etc/bashrc ] && source /etc/bashrc
[ -f /etc/bash.bashrc ] && source /etc/bash.bashrc

# Adjust the prompt depending on whether we're in 'guix environment'.
if [ -n "$GUIX_ENVIRONMENT" ]
then
    PS1='\u@\h \w [env]\$ '
else
    PS1='\u@\h \w\$ '
fi

# Alias definitions
alias ls='ls -p --color=auto'
alias ll='ls -l'
alias grep='grep --color=auto'
alias minimal_emacs='emacs -Q --load ~/.config/emacs/minimal-init.el'
# Conditional alias definitions
[ -x "$(command -v kitty)" ] && alias ssh="kitty +kitten ssh"
[ -x "$(command -v nvim)" ] && alias vim=nvim

# Local settings
[ -f "${HOME}/.bashrc_local" ] && source "${HOME}/.bashrc_local"

# direnv
if [ -x "$(command -v direnv)" ]; then
  eval "$(direnv hook bash)"
fi

# Return if dumb shell
if [ "$TERM" = "dumb" ]
then
    return
fi

# fzf
if [ -x "$(command -v fzf)" ]; then
    [ -f "$HOME"/.config/fzf/completion.bash ] && source "$HOME"/.config/fzf/completion.bash
    [ -f "$HOME"/.config/fzf/key-bindings.bash ] && source "$HOME"/.config/fzf/key-bindings.bash
fi

# Emacs vterm
if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
    function clear(){
        vterm_printf "51;Evterm-clear-scrollback";
        tput clear;
    }
fi

vterm_printf(){
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ] ); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

vterm_prompt_end(){
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
}
PS1=$PS1'\[$(vterm_prompt_end)\]'
