#!/bin/sh
## This file should be automatically sourced by the login manager or Bash if
## .bash_profile does not exist.  If this file is not automatically sourced,
## do it from the shell config to me sure it applies to TTY as well.

## Preliminary path definitions. For security reasons (and bad programming
## assumptions) you should always append entries to PATH, not prepend them.
appendpath () {
    [ $# -eq 2 ] && PATHVAR=$2 || PATHVAR=PATH
    [ -d "$1" ] || return
    eval echo \$$PATHVAR | grep -q "\(:\|^\)$1\(:\|$\)" && return
    eval export $PATHVAR="\$$PATHVAR:$1"
}
prependpath () {
    [ $# -eq 2 ] && PATHVAR=$2 || PATHVAR=PATH
    [ -d "$1" ] || return
    eval echo \$$PATHVAR | grep -q "\(:\|^\)$1\(:\|$\)" && return
    eval export $PATHVAR="$1:\$$PATHVAR"
}

# Environmental variables
export XDG_CONFIG_HOME="${HOME}/.config"
export NOTMUCH_CONFIG="${HOME}/.config/notmuch/config"
export EMAIL=niklas.eklund@posteo.net
export NAME="Niklas Eklund"
export EDITOR=emacsclient
export BROWSER=nyxt
export PAGER=less
export GUIX_EXTRA_PROFILES="${HOME}/.guix-extra-profiles"
GPG_TTY="$(tty)"
export GPG_TTY
export GNUPGHOME="${HOME}/.gnupg"
export FZF_DEFAULT_OPTS='--color bw'
export THEME="light"

## Guix
if [[ $DESKTOP_SESSION == *"gnome" ]]; then
    declare -a guix_extra_profiles=("${HOME}/.guix-extra-profiles/emacs" "${HOME}/.guix-extra-profiles/emacs-native-comp" "${HOME}/.guix-extra-profiles/main" "${HOME}/.guix-extra-profiles/guixbook")
else
    declare -a guix_extra_profiles="${HOME}/.guix-extra-profiles/"*
fi

for i in ${guix_extra_profiles[@]}; do
    profile=$i/$(basename "$i")
    if [ -f "$profile"/etc/profile ]; then
    GUIX_PROFILE="$profile" ; . "$profile"/etc/profile
        export MANPATH="$profile"/share/man:$MANPATH
        export INFOPATH="$profile"/share/info:$INFOPATH
        export XDG_DATA_DIRS="$profile"/share:$XDG_DATA_DIRS
        export XDG_CONFIG_DIRS="$profile"/etc/xdg:$XDG_CONFIG_DIRS
    fi
    unset profile
done

## Nix
if [ -x "$(command -v nix)" ]; then
    . /run/current-system/profile/etc/profile.d/nix.sh
fi

## Non Guix System
if [ -n "$(uname -a | grep "Ubuntu")" ]; then
    # To get all the right Guile dependencies guix itself is
    # installed. But we want the system installed Guix binary to be
    # used.
    export PATH="${HOME}/.config/guix/current/bin":$PATH
    # Guix packages should not use system locales
    export GUIX_LOCPATH="${HOME}/.guix-extra-profiles/ubuntu/ubuntu/lib/locale"
    # Export variables for SSL
    export SSL_CERT_DIR="${HOME}/.guix-profile/etc/ssl/certs"
    export SSL_CERT_FILE="${HOME}/.guix-profile/etc/ssl/certs/ca-certificates.crt"
fi

## Last PATH entries.
appendpath "${HOME}/.local/bin"
appendpath "${HOME}/.local/share/flatpak/exports/share/wrappers"

## Linux specific
if [ "$(uname -o)" = "GNU/Linux" ] ; then
    ## Startup error log.
    log_dmesg="$(dmesg | grep -i error)"
    [ -n "$log_dmesg" ] && echo "$log_dmesg" > "${HOME}/errors-dmesg.log" || rm "${HOME}/errors-dmesg.log" 2>/dev/null
    unset log_dmesg
fi

## Local profile
[ -f "${HOME}/.profile_local" ] && . "${HOME}/.profile_local"

## Bash
[ "$(ps -o comm= $$)" != bash ] && return
[ -f "${HOME}/.bashrc"  ] && . "${HOME}/.bashrc"
